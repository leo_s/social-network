import {
    FETCH_USER,
    AUTO_LOGIN,
    SHOW_PROFILE,
    LOGOUT,
    LOAD_USER_PROFILE,
} from '../actions/types';

const initialState = {
    authTag: localStorage.getItem('isAuth'),
    myId: localStorage.getItem('myId'),
}

export default function(state = initialState, action) {
    switch (action.type) {
        case FETCH_USER:
            localStorage.setItem('isAuth', 'true');
            localStorage.setItem('myId', action.payload.id);
            return {
                authTag: 'true',
            }
        case AUTO_LOGIN:
            return {
                authTag: action.payload
            }
        case SHOW_PROFILE:
            return {
                authTag: action.payload
            }
        case LOGOUT:
            localStorage.setItem('isAuth', 'false');
            return {
                authTag: 'false'
            }
        case LOAD_USER_PROFILE:
            return {
                userProfile: action.payload,
                authTag: localStorage.getItem('isAuth'),
                myId: localStorage.getItem('myId')
            }
        default:
            return state;
    }
}