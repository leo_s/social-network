import {
    CREATE_POST,
    POST_LIST,
} from '../actions/types';

const initialState = {
    authTag: localStorage.getItem('isAuth'),
    myId: localStorage.getItem('myId'),
}

export default function (state = initialState, action) {
    switch (action.type) {
        case CREATE_POST:
            return {
                report: action.payload,
                authTag: localStorage.getItem('isAuth'),
                myId: localStorage.getItem('myId')
            }
        case POST_LIST:
            return {
                postsList: action.payload.rows,
                postsQuantity: action.payload.count,
                authTag: localStorage.getItem('isAuth'),
                myId: localStorage.getItem('myId')
            }
        default:
            return state;
    }
}