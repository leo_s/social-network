import {
    CREATE_PROFILE,
    LOAD_PROFILE,
    EDIT_PROFILE,
} from '../actions/types';

const initialState = {
    authTag: localStorage.getItem('isAuth'),
    myId: localStorage.getItem('myId'),
}

export default function(state = initialState, action) {
    switch (action.type) {
        case CREATE_PROFILE:
            return action.payload;
        case EDIT_PROFILE:
            return state;
        case LOAD_PROFILE:
            return {
                formData: action.payload,
                authTag: localStorage.getItem('isAuth')
            };
        default:
            return state;
    }
}
