//TODO how to install to !!! cd ./frontend 'npm install --save redux-form' bu package.json?
import { combineReducers } from 'redux';
import { reducer as reduxForm } from 'redux-form';
import authReducer from './authReducer';
import profileReducer from './profileReducer';
import friendReducer from './friendReducer';
import postReducer from './postReducer';

export default combineReducers({
    auth: authReducer,
    profile: profileReducer,
    friend: friendReducer,
    post: postReducer,
    form: reduxForm,
});
