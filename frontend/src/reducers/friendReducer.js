import {
    FETCH_USER,
    CREATE_USER,
    AUTO_LOGIN,
    SHOW_PROFILE,
    LOGOUT,
    LOAD_PROFILE,
    EDIT_PROFILE,
    USERS_LIST_FILTER,
    USERS_LIST,
    CONF_FRIENDS_LIST,
    NON_CONF_FRIENDS_LIST,
    // MY_FRIEDS,
    QUERY_TO_ME_LIST,
    LOAD_USER_PROFILE,
    CREATE_POST,
    POST_LIST,
} from '../actions/types';

const initialState = {
    authTag: localStorage.getItem('isAuth'),
    myId: localStorage.getItem('myId'),
}

export default function(state = initialState, action) {
    switch (action.type) {
        case USERS_LIST_FILTER:
            // debugger
            return {
                usersList: action.payload,
                previousFormValues: action.previousFormValues,
                authTag: localStorage.getItem('isAuth'),
                myId: localStorage.getItem('myId')
            }
        case USERS_LIST:
            // console.log(action.payload)
            return {
                usersList: action.payload,
                authTag: localStorage.getItem('isAuth'),
                myId: localStorage.getItem('myId')
            }
        case CONF_FRIENDS_LIST:
            // debugger
            return {
                confFriendsList: action.payload.count > 0 ?
                    action.payload.rows :
                    [{id: 0, name: "emptyConfFriendsList", surname: "", master_id: 0, confirmed: 0}],
                // recordsQuantity: action.payload.count,
                recordsQuantity: 1,
                activeItem: 1,
                authTag: localStorage.getItem('isAuth'),
                myId: localStorage.getItem('myId')
            }
        case NON_CONF_FRIENDS_LIST:
            // debugger
            return {
                nonConfFriendsList: action.payload.count > 0 ?
                    action.payload.rows :
                    [{id: 0, name: "emptyConfFriendsList", surname: "", master_id: 0, confirmed: 0}],
                recordsQuantity: action.payload.count,
                activeItem: 2,
                authTag: localStorage.getItem('isAuth'),
                myId: localStorage.getItem('myId')
            }
        case QUERY_TO_ME_LIST:
            return {
                queryToMe: action.payload.count > 0 ?
                    action.payload.rows :
                    [{id: 0, name: "emptyConfFriendsList", surname: "", master_id: 0, confirmed: 0}],
                recordsQuantity: action.payload.count,
                activeItem: 3,
                authTag: localStorage.getItem('isAuth'),
                myId: localStorage.getItem('myId')
            }
        default:
            return state;
    }
}