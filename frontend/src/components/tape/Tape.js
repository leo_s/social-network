import _ from 'lodash';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {postsListAction} from '../../actions/post';
import faker from 'faker';

class Tape extends Component {
    constructor(props) {
        super(props);
        this.state = {currentPage: 0};
    }

    renderPagination() {
        let children = []
        for (let page = 0; page < this.props.postsQuantity/5; page++) {
            children.push(<li className={this.state.currentPage == page ? "active" : "waves-effect"} key={page}>
                <a href="#!" onClick={this.paginationOnClick.bind(this, this.props.postsListAction, page)}>{page}</a>
            </li>)
        }
        return children;
    }

        renderItems() {
        return _.map(this.props.postsList, ({user_id, name, surname, created_at, post}) => {
            const key = Math.floor(Math.random() * 100000) + 1;
            const d = new Date(created_at);
            var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                return (
                    <tr key={key}>
                        <td>
                    <div className="row">
                    <div className="col s12 m2">
                        <img style={{"width": "50px"}} src={faker.image.avatar()} alt="avatar" className="circle"/>
                    </div>
                    <div className="col s12 m10">
                        <span className="title"><Link to={`/showProfile/${user_id}`}>{name} {surname}</Link></span>
                        <p><label>{d.getDate()} {months[d.getMonth()]} {d.getFullYear()}</label></p>
                        <p>{post}</p>
                    </div>
                    </div>
                        </td>
                    </tr>
                );
        });
    }

    componentDidMount() {
        this.props.postsListAction(this.state.currentPage)
            .catch((error) => {
                debugger;
                if (error.response.data.localeCompare('Unauthenticated') === 0) {
                    this.props.history.push("/login")
                }
            })
    }

    paginationOnClick(action, id){
        this.setState({currentPage: id})
        action(id)
            .catch((error) => {
                debugger;
                if (error.response.data.localeCompare('Unauthenticated') === 0) {
                    this.props.history.push("/login")
                }
            })
    }

    render() {
        return (
            <main role="main" id="MainContent">
                <div className="section container">
                    <div className="row">
                        <div className="col s12 m6 offset-m3">
                            <div className="card login-wrapper">
                                <div className="card-content">
                                    <table className="bordered">
                                        <thead>
                                        <tr>
                                            <th>The posts of my friends
                                            <ul className="pagination">
                                                <li className={this.state.currentPage == 0 ? "disabled" : "waves-effect"} key='100'>
                                                    <a href="#!"
                                                       onClick={this.paginationOnClick.bind(this, this.props.postsListAction, this.state.currentPage-1)}
                                                    style={this.state.currentPage == 0 ? {pointerEvents: "none"} : {pointerEvents: "auto"}}><i
                                                    className="material-icons">chevron_left</i></a></li>
                                                {this.renderPagination()}
                                                <li className={this.state.currentPage < this.props.postsQuantity/5-1 ? "waves-effect": "disabled"} key='101'><a href="#!"
                                                onClick={this.paginationOnClick.bind(this, this.props.postsListAction, this.state.currentPage+1)}
                                                style={this.props.postsQuantity/5-1 > this.state.currentPage ? {pointerEvents: "auto"} : {pointerEvents: "none"}}><i
                                                    className="material-icons">chevron_right</i></a></li>
                                            </ul>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {this.renderItems()}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        );
    }
}

function mapStateToProps({post}) {
    return {
        postsList: post.postsList,
        postsQuantity: post.postsQuantity,
        myId: post.myId
    }
}

export default connect(
    mapStateToProps,
    { postsListAction,
        // usersListAction
    },
)(Tape);
