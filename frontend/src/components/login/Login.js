//Signup shows a form for a user registration
//TODO 40h/19/224 npm install --save react-redux@5.1.1
//to avoid message like this: withRef is removed. To access the wrapped instance, use a ref on the connected component
import _ from 'lodash';
import React, {Component} from 'react';
import {reduxForm, Field} from 'redux-form';
import {connect} from 'react-redux';
import LoginField from './LoginField';
import validateEmails from '../../utils/validateEmails';
import { loginAction } from '../../actions';


const FIELDS = [
    {label: 'Enter your e-mail', name: 'email', noValueError: 'Provide an e-mail', type: 'text'},
    {label: 'Enter password', name: 'password', noValueError: 'Provide password', type: 'password'},
];

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {loginWarning: ''};
    }

    renderFields() {
        return _.map(FIELDS, ({label, name, type}) => {
            return (
                <Field
                    key={name}
                    component={LoginField}
                    type={type} label={label}
                    name={name}
                />
            );
        });
    }

    onSubmit(values) {
        this.props.loginAction(values)
            .then(() => {
                // debugger;
                this.props.history.push("/")})
            .catch((error) => {
                // debugger;
                if (error.response.data.localeCompare('Incorrect login/password') === 0){
                    this.setState({
                        loginWarning: 'Incorrect login/password'
                    })
                }
            })
    }

    render() {
        return (
            <main role="main" id="MainContent">
                <div className="section container">
                    <div className="row">
                        <div className="col s12 m6 offset-m3">
                            <div className="card login-wrapper">
                                <div className="card-content">
                                    <form onSubmit={this.props.handleSubmit(this.onSubmit.bind(this))}>
                                        <h4 className="center">Login</h4>
                                        <div>
                                            <span className="red-text text-darken-2">{this.state.loginWarning}</span>
                                        </div>
                                        {this.renderFields()}
                                        <button type="submit" className="teal btn-flat white-text" style={{'marginTop': '10px'}}>
                                            LOGIN
                                            <i className="material-icons right">done</i>
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        );
    }
}

function validate(values) {
    const errors = {};

    errors.email = validateEmails(values.email || '');

    _.each(FIELDS, ({name, noValueError}) => {
        if (!values[name]) {
            errors[name] = noValueError;
        }
    });
    return errors;
}

export default connect(
    null,
    { loginAction },
)(reduxForm({
    validate,
    form: 'signup'
})(Login));
