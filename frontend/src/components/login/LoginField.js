import React from 'react';

export default ({input, label, type, meta:{ error, touched }}) => {
    return (
        <div className="input-field">
            <label>{label}</label>
            <input {...input} type={type} style={{margin: 0}}/>
            <div className="red-text">
                {touched && error}
            </div>
        </div>
    );
};
