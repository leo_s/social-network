import React, {Component} from 'react';
import {connect} from 'react-redux';
import {logoutAction} from '../../actions';
import {Field} from "redux-form";
import LoginField from "../login/LoginField";

class Logout extends Component {
    renderFields() {
        return _.map(FIELDS, ({label, name, type}) => {
            return (
                <Field
                    key={name}
                    component={LoginField}
                    type={type} label={label}
                    name={name}
                />
            );
        });
    }

componentDidMount (){
    this.props.logoutAction()
        .then(() => {
            this.props.history.push("/")})
}

    render() {
        return (
            <div></div>);
    }
}

export default connect(null,{ logoutAction })(Logout);