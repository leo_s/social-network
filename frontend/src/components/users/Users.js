import _ from 'lodash';
import React, {Component} from 'react';
import {reduxForm, Field} from 'redux-form';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import { usersListFilter, usersListAction, sendFriendQueryAction } from '../../actions/friends';
import faker from 'faker';
import SearchField from './SearchField'

const onChange = (values, dispatch, props, previousValues) => {
    // debugger
dispatch(usersListFilter(values))
}

function FriendAddButton(props) {
    return (
        <div style={{'float': 'right'}}>
            <a className="waves-effect waves-light btn" onClick={props.onClick}
               style={{'width': '160px'}}>
                add friend
            </a>
        </div>
    );
}

function InactiveButton(props) {
    return (
        <div style={{'float': 'right'}}>
            <a className="btn disabled" style={{'width': '160px'}}>{props.name}</a>
        </div>
    );
}

class Users extends Component {
    constructor(props) {
        super(props);
        this.state = {
            usersListFilter: 'Gek Sandors',
        };
    }

    renderFields() {
        return (
                <Field
                    key="keyName"
                    name="firstName"
                    component={SearchField}
                    type="text"
                    placeholder="Placeholder First Name"
                />
        );
    }

    renderItems() {
        return _.map(this.props.usersList, ({id, name, surname, confirmed, masterId}) => {
            let button;
            if (masterId === undefined){
                button = <div></div>
            } else {
                if (masterId === null) {
                    button = <FriendAddButton onClick={this.props.handleSubmit(this.onClick.bind(this, id))}/>;
                } else if (confirmed === 0) {
                    button = <InactiveButton name='del Request'/>;
                } else {
                    button = <InactiveButton name='del friend'/>;
                }
            }
            if(this.props.myId != id){
            return (
                <li className="collection-item avatar" key={id}>
                    <div style={{'float': 'left'}}>
                        <img src={faker.image.avatar()} alt="avatar" className="circle"/>
                        <span className="title"><Link to={`/showProfile/${id}`}>{name} {surname}</Link></span>
                        <p style={{'width': '250px'}}>School: {faker.address.city()}, {faker.address.streetAddress()}</p>
                    </div>
                    {button}
                </li>

            );}
        });
    }

    componentDidMount() {
        this.props.usersListAction()
            .catch((error) => {
                debugger;
                if (error.response.data.localeCompare('Unauthenticated') === 0) {
                    this.props.history.push("/login")
                }
            })
    }

    onClick(id){
        // debugger
        this.props.sendFriendQueryAction(id, this.props.previousFormValues)
            .catch((error) => {
                debugger;
                if (error.response.data.localeCompare('Unauthenticated') === 0) {
                    this.props.history.push("/login")
                }
            })

    }

    render() {
        return (

            <main role="main" id="MainContent">
                <div className="section container">
                    <div className="row">
                        <div className="col s12 m6 offset-m3">
                            <ul className="collection">
                                <li className="collection-item">
                                    <form style={{'marginBottom': '0px'}}>
                                        {this.renderFields()}
                                    </form>
                                </li>
                                {this.renderItems()}
                            </ul>
                        </div>
                    </div>
                </div>
            </main>
        );
    }
}

function mapStateToProps({friend}) {
    return {
        usersList: friend.usersList,
        previousFormValues: friend.previousFormValues,
        myId: friend.myId
    }
}

export default connect(
    mapStateToProps,
    { usersListFilter, usersListAction, sendFriendQueryAction},
)(reduxForm({
    form: 'search',
    onChange
})(Users));