import React from 'react';

export default ({input, label, type, meta:{ error, touched }}) => {
    return (
        <div className="input-field" style={{margin: 0}}>
            {/*<label>{label}</label>*/}
            <i className="red-text material-icons prefix">search</i>
            <input {...input} type={type} placeholder="search friends" style={{'marginBottom': '0px', 'height': '30px', 'width': '250px'}}/>
            <div className="red-text">
                {touched && error}
            </div>
        </div>
    );
};