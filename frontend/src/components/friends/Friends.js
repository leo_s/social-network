import _ from 'lodash';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {
    confirmedFriendsListAction,
    nonConfirmedFriendsListAction,
    queryToMeListAction,
    sendFriendConfirmAction,
    friendDeleteAction,
    queryDeleteAction,
} from '../../actions/friends';
import faker from "faker";

function Button(props) {
    return (
        <div style={{'float': 'right'}}>
            <a className={props.className} onClick={props.onClick}
               style={{'width': '160px'}}>
                {props.buttonName}
            </a>
        </div>
    );
}

class Friends extends Component {
    constructor(props) {
        super(props);
        this.state = {loginWarning: ''};
    }


    renderFriendship(list, quantity, action) {
        return _.map(list, ({id, name, surname, confirmed, master_id}) => {
            if (name.localeCompare('emptyConfFriendsList') == 0) {
                let message;
                let button = <Button onClick={this.pushOnClick.bind(this)}
                                     buttonName={'search'}
                                     className='waves-effect waves-light btn'/>;

                switch (action) {
                    case 'SHOW_FRIEND':
                        message = 'You do not have any friend';
                        break
                    case 'SHOW_MY_QUERY':
                        message = 'You have not sent any query';
                        break
                    default:
                        button = ''
                        message = 'You have not been queried for friendship';
                }
                return (
                            <li className="collection-item avatar" key={id} style={{'paddingLeft': '25px'}}>
                                <div style={{'float': 'left'}}>
                                    <h5>{message}</h5>
                                </div>
                                {button}
                            </li>
                )
            } else {
                let button;
                switch (action) {
                    case 'SHOW_FRIEND':
                        button = <Button onClick={this.buttonOnClick.bind(this, this.props.friendDeleteAction, id)}
                                         buttonName={'del friend'}
                                         className='waves-effect waves-light btn red darken-4'/>;
                        break
                    case 'SHOW_MY_QUERY':
                        button = <Button onClick={this.buttonOnClick.bind(this, this.props.queryDeleteAction, id)}
                                         buttonName={'del query'}
                                         className='waves-effect waves-light btn red darken-4'/>;
                        break
                    default:
                        button = <Button onClick={this.buttonOnClick.bind(this, this.props.sendFriendConfirmAction, id)}
                                         buttonName='accept'
                                         className='waves-effect waves-light btn'/>;
                }
                return (
                            <li className="collection-item avatar" key={id} >
                                <div style={{'float': 'left'}}>
                                    <img src={faker.image.avatar()} alt="avatar" className="circle"/>
                                    <span className="title"> <Link
                                        to={`/showProfile/${id}`}>{name} {surname}</Link> </span>
                                    <p style={{'width': '280px'}}>School: {faker.address.city()}, {faker.address.streetAddress()}</p>
                                </div>
                                {button}
                            </li>
                );
            }
        });
    }

    renderFriendMenu(activeItem){
        let classNameOne = '';
        let classNameTwo = '';
        let classNameThree = '';
        switch (activeItem) {
            case 1:
                classNameOne = 'active';
                break
            case 2:
                classNameTwo = 'active';
                break
            default:
                classNameThree = 'active';
        }
        return(
            <ul className="tabs tabs-fixed-width grey lighten-5">
                <li className="tab" key='m1'>
                    <Link className={classNameOne} to="#confirmed"
                          onClick={this.menuOnClick.bind(
                              this, this.props.confirmedFriendsListAction)}>
                        My friends
                    </Link>
                </li>
                <li className="tab" key='m2'>
                    <Link className={classNameTwo} to="#nonConfirmed"
                          onClick={this.menuOnClick.bind(
                              this, this.props.nonConfirmedFriendsListAction)}>
                        My requests
                    </Link>
                </li>
                <li className="tab" key='m3'>
                    <Link className={classNameThree} to="#queryToMe"
                          onClick={this.menuOnClick.bind(
                              this, this.props.queryToMeListAction)}>
                        Requests to me
                    </Link>
                </li>
            </ul>
        )
    }

    pushOnClick() {
        this.props.history.push("/users")
    }

    buttonOnClick(action, id) {
        // debugger
        action(id)
        // this.props.sendFriendConfirmAction(id)
            .catch((error) => {
                debugger;
                if (error.response.data.localeCompare('Unauthenticated') === 0) {
                    this.props.history.push("/login")
                }
            })
    }

    menuOnClick(action) {
        action()
            .catch((error) => {
                if (error.response.data.localeCompare('Unauthenticated') === 0) {
                    this.props.history.push("/login")
                }
            })
    }

    componentDidMount() {
        this.props.confirmedFriendsListAction()
            .catch((error) => {
                if (error.response.data.localeCompare('Unauthenticated') === 0) {
                    this.props.history.push("/login")
                }
            })
    }

    render() {
        return (
            <main role="main" id="MainContent">
                <div className="section container">
                    <div className="row">
                        <div className="col s12 m6 offset-m3">
                            <div className="row" style={{'marginBottom': '0px'}}>
                                {this.props.activeItem != undefined &&
                                <div className="card-tabs">
                                    {this.renderFriendMenu(this.props.activeItem)}
                                </div>
                                }
                            </div>
                            <div className="row">
                                <ul className="collection" style={{'margin': '0px'}}>
                                <div id="confirmed">
                                    {this.renderFriendship(
                                        this.props.confFriendsList,
                                        this.props.recordsQuantity,
                                        'SHOW_FRIEND'
                                    )}
                                </div>
                                <div id="nonConfirmed">
                                    {this.renderFriendship(
                                        this.props.nonConfFriendsList,
                                        this.props.recordsQuantity,
                                        'SHOW_MY_QUERY')}
                                </div>
                                <div id="queryToMe">
                                    {this.renderFriendship(
                                        this.props.queryToMeList,
                                        this.props.recordsQuantity,
                                        'CONFIRM_QUERY')}
                                </div>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        );
    }
}

function mapStateToProps({friend}) {
    return {
        confFriendsList: friend.confFriendsList,
        nonConfFriendsList: friend.nonConfFriendsList,
        queryToMeList: friend.queryToMe,
        recordsQuantity: friend.recordsQuantity,
        activeItem: friend.activeItem
    }
}

export default connect(
    mapStateToProps,
    {
        confirmedFriendsListAction,
        nonConfirmedFriendsListAction,
        sendFriendConfirmAction,
        friendDeleteAction,
        queryDeleteAction,
        queryToMeListAction
    },
)(Friends)
