import React from 'react';

export default ({input, label, type, meta:{ error, touched }}) => {
    return (
        <div className="input-field">
            <i className="material-icons prefix">mode_edit</i>
            <label htmlFor="textarea2">{label}</label>
            <textarea id="textarea2" className="materialize-textarea" {...input} type={type}></textarea>
            <div className="red-text">
                {touched && error}
            </div>
        </div>
    );
};
