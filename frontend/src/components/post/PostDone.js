import React from 'react';

const SuccessSignup = () => {
    return (
        <div className="section container">
            <div className="row">
                <div className="col s12 m6 offset-m3">
                    <div className="card" style={{'paddingBottom': '25px'}}>
                        <div className="card-content black-text">
                            <span className="card-title center">Your post is written.</span>
                        </div>
                        <div className= "center">
                            <a className="btn-floating"><i className="material-icons">done</i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default SuccessSignup;