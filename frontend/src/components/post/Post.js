import _ from 'lodash';
import React, {Component} from 'react';
import {reduxForm, Field} from 'redux-form';
import {connect} from 'react-redux';

import PostField from './PostField';
import validatePost from '../../utils/validatePost';
import { createPostAction } from '../../actions/post';

const FIELDS = [
    {label: 'Type you post here', name: 'post', noValueError: 'Provide your post', type: 'text'},
];

class Post extends Component {

    renderFields() {
        return _.map(FIELDS, ({label, name, type}) => {
            return (
                <Field
                    key={name}
                    component={PostField}
                    type={type} label={label}
                    name={name}
                />
            );
        });
    }

    onSubmit(values) {
        this.props.createPostAction(values)
            .then(() => {
                this.props.history.push("/postDone")})
            .catch((error) => {
                if (error.response.data.localeCompare('Incorrect login/password') === 0){
                    this.setState({
                        loginWarning: 'Incorrect login/password'
                    })
                }
            })
    }

    render() {
        return (
            <main role="main" id="MainContent">
                <div className="section container">
                    <div className="row">
                        <div className="col s12 m6 offset-m3">
                            <div className="input-field">
                                    <form onSubmit={this.props.handleSubmit(this.onSubmit.bind(this))}>
                                        {this.renderFields()}
                                        <button type="submit" className="teal btn-flat white-text" style={{'marginTop': '10px'}}>
                                            SUBMIT
                                            <i className="material-icons right">send</i>
                                        </button>
                                    </form>
                                </div>
                        </div>
                    </div>
                </div>
            </main>
        );
    }
}

function validate(values) {
    const errors = {};
    errors.post = validatePost(values.post || '');

    _.each(FIELDS, ({name, noValueError}) => {
        if (!values[name]) {
            errors[name] = noValueError;
        }
    });
    return errors;
}

export default connect(
    null,
    { createPostAction },
)(reduxForm({
    validate,
    form: 'signup'
})(Post));
