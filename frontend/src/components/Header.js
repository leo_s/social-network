import React, { Component } from 'react';
import {connect} from 'react-redux';
import {Link} from "react-router-dom";
import {autoLogin} from "../actions";

class Header extends Component {
    renderContent() {
        const login = this.props.auth.authTag;
        switch (login) {
            case 'true':
                return [
                    <li key="2">
                        <Link to="/tape" className="tape">Tape</Link>
                    </li>,
                    <li key="3">
                        <Link to="/post" className="post">Add post</Link>
                    </li>,
                    <li key="8">
                        <Link to="/friends" className="post">Friends</Link>
                    </li>,
                    <li key="4">
                        <Link to="/users" className="users">Users</Link>
                    </li>,
                    <li key="5">
                        <Link to="/editProfile" className="profile">Edit Profile</Link>
                    </li>,
                    <li key="7">
                        <Link to="/logout" id="logout">Logout</Link>
                    </li>,
                ];
            default:
                return [
                    <li key="4">
                        <Link to="/login" id="login">Login</Link>
                    </li>,
                    <li key="5">
                        <Link to="/signup" id="create_account">Create account</Link>
                    </li>,
                ];
        }
    }

    componentDidMount() {
        this.props.autoLogin();
    }

    render() {
        return (
            <nav className="nav-extended teal">
                <div className="nav-wrapper container">
                    <Link to="/" itemProp="url" className="brand-logo site-logo">
                        Thumbtack Social-network
                    </Link>
                    <ul className="right hide-on-med-and-down">
                        {this.renderContent()}
                    </ul>
                </div>

            </nav>
        );
    }
}

function mapStateToProps({auth}) {
    return { auth }
}

function mapDispatchToProps(dispatch) {
    return {
        autoLogin: (() => dispatch(autoLogin()))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
