import _ from 'lodash';
import React, {Component} from 'react';
import {reduxForm, Field} from 'redux-form';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import SignupField from './SignupField';
import validateEmails from '../../utils/validateEmails';
import validatePass from '../../utils/validatePass';
import { createProfileAction } from '../../actions/profile';

const FIELDS = [
    {label: 'Enter your name', name: 'name', noValueError: 'Provide you name', type: 'text'},
    {label: 'Enter your surname', name: 'surname', noValueError: 'Provide you surname', type: 'text'},
    {label: 'Enter your e-mail', name: 'email', noValueError: 'Provide an e-mail', type: 'text'},
    {label: 'Enter password', name: 'password', noValueError: 'Provide password', type: 'password'},
    {label: 'Repeat password', name: 'repPassword', noValueError: 'Repeat password', type: 'password'},
];

class Signup extends Component {
    constructor(props) {
        super(props);
        this.state = {signupWarning: ''};
    }

    renderFields() {
        return _.map(FIELDS, ({label, name, type}) => {
            return (
                <Field
                    key={name}
                    component={SignupField}
                    type={type}
                    label={label}
                    name={name}
                />
            );
        });
    }

    onSubmit(values) {
        this.props.createProfileAction(values)
            .then(() => {
                this.props.history.push("/signupDone")
                //TODO for redirection with page reload
                // document.location.href = "/login";
                // history.push does not reload
            })
            .catch((error) => {
                this.setState({
                    signupWarning: error.response.data})
            })
    }

    render() {
        return (
            <main role="main" id="MainContent">
                <div className="section container">
                    <div className="row">
                        <div className="col s12 m6 offset-m3">
                            <div className="card login-wrapper">
                                <div className="card-content">
                                    <form onSubmit={this.props.handleSubmit(this.onSubmit.bind(this))}>
                                            <h4 className="center">Create Account</h4>
                                        <div>
                                            <span className="red-text text-darken-2">{this.state.signupWarning}</span>
                                        </div>
                                        {this.renderFields()}
                                        <Link to="/" className="red btn-flat white-text" style={{'marginTop': '10px'}}>
                                            Cansel
                                        </Link>
                                        <button type="submit" className="teal btn-flat right white-text" style={{'marginTop': '10px'}}>
                                            Submit
                                            <i className="material-icons right">done</i>
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        );
    }
}

function validate(values) {
    const errors = {};

    errors.email = validateEmails(values.email || '');
    errors.password = validatePass.complexity(values.password || '');
    errors.repPassword = validatePass.compare((values.repPassword || ''), (values.password || ''));

    _.each(FIELDS, ({name, noValueError}) => {
        if (!values[name]) {
            errors[name] = noValueError;
        }
    });
    return errors;
}

// function mapStateToProps({auth}) {
//     return { auth }
// }
//
export default connect(
    null,
    { createProfileAction },
)(reduxForm({
    validate,
    form: 'signup'
})(Signup));
