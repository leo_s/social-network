import React from 'react';
import {Link} from "react-router-dom";

const SuccessSignup = () => {
    return (
        <div className="section container">
            <div className="row">
                <div className="col s12 m6 offset-m3">
                    <div className="card">
                        <div className="card-content black-text">
                            <span className="card-title center">You have successfully registered!</span>
                        </div>
                        <div className="center">
                            <a className="btn-floating"><i className="material-icons">done</i></a>
                        </div>
                        <div className="card-content center">
                            <p><Link to="/login">Login</Link></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default SuccessSignup;