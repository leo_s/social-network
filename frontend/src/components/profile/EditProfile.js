import _ from 'lodash';
import React, {Component} from 'react';
import {reduxForm, Field} from 'redux-form';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import ProfileField from './ProfileField';
import validateEmails from '../../utils/validateEmails';
import {editProfileAction, loadProfileAction} from '../../actions/profile';

const FIELDS = [
    {label: 'Name', name: 'name', noValueError: 'Provide you name', type: 'text'},
    {label: 'Surname', name: 'surname', noValueError: 'Provide you surname', type: 'text'},
    {label: 'e-mail', name: 'email', noValueError: 'Provide an e-mail', type: 'text'},
];

class ShowProfile extends Component {
    renderForm() {
        return _.map(FIELDS, ({label, name, type}) => {
            return (
                <Field
                    key={name}
                    component={ProfileField}
                    type={type} label={label}
                    name={name}
                />
            );
        });
    }

    onSubmit(values) {
        this.props.editProfileAction(values)
            .then(() => {
                this.props.history.push("/editProfileDone")
            })
    }

    componentDidMount() {
        this.props.loadProfileAction()
            .catch((error) => {
                // debugger;
                if (error.response.data.localeCompare('Unauthenticated') === 0) {
                    this.props.history.push("/login")
                }
            })
    }

    render() {
        return (
            <main role="main" id="MainContent">
                <div className="section container">
                    <div className="row">
                        <div className="col s12 m6 offset-m3">
                            <div className="card login-wrapper">
                                <div>
                                    <h4 className="center" style={{'paddingTop': '10px', 'marginBottom': '0px'}}>Edit profile</h4>
                                </div>
                                <div id="Basic">
                                    <div className="card-content">
                                        <form
                                            onSubmit={this.props.handleSubmit(this.onSubmit.bind(this))}>
                                            {this.renderForm()}
                                            <Link to="/" className="red btn-flat white-text red darken-4" style={{'marginTop': '10px'}}>
                                                Cansel
                                            </Link>
                                            <button type="submit" className="teal btn-flat right white-text" style={{'marginTop': '10px'}}>
                                                Submit
                                                <i className="material-icons right">done</i>
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        );
    }
}

function validate(values) {
    const errors = {};
    errors.email = validateEmails(values.email || '');
    _.each(FIELDS, ({name, noValueError}) => {
        if (!values[name]) {
            errors[name] = noValueError;
        }
    });
    return errors;
}

function mapStateToProps(state) {
    return {
        error: state.auth.error,
        initialValues: state.profile.formData,
    }
}

export default connect(
    mapStateToProps,
    {editProfileAction, loadProfileAction},
)(reduxForm({
    validate,
    form: 'profile',
    enableReinitialize : true
})(ShowProfile));
