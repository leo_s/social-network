import React, {Component} from 'react';
import {connect} from 'react-redux';
import {loadUserProfileAction, usersList as usersListAction} from '../../actions';
import faker from "faker";


class ShowProfile extends Component {

    renderItems() {
        return _.map(this.props.usersList, ({id, name, surname, confirmed, masterId}) => {
            let button;
            if (masterId === undefined) {
                button = <div></div>
            } else {
                if (masterId === null) {
                    button = <FriendAddButton onClick={this.props.handleSubmit(this.onClick.bind(this, id))}/>;
                } else if (confirmed === 0) {
                    button = <InactiveButton name='del Request'/>;
                } else {
                    button = <InactiveButton name='del friend'/>;
                }
            }
            if (this.props.myId != id) {
                return (
                    <li className="collection-item avatar" key={id}>
                        <div style={{'float': 'left'}}>
                            <img src={faker.image.avatar()} alt="avatar" className="circle"/>
                            <span className="title"> <a href="#">{name} {surname}</a> </span>
                            <p style={{'width': '250px'}}>School: {faker.address.city()}, {faker.address.streetAddress()}</p>
                        </div>
                        {button}
                    </li>

                );
            }
        });
    }

    componentDidMount() {
        this.props.loadUserProfileAction(this.props.match.params.userId)
            .catch((error) => {
                debugger;
                if (error.response.data.localeCompare('Unauthenticated') === 0) {
                    this.props.history.push("/login")
                }
            })
    }

    render() {
        return (
            <main role="main" id="MainContent">
                <div className="section container">
                    <div className="row" style={{'marginBottom': '0px'}}>
                        <div className="col s12 m6 offset-m3">
                            <div className="card horizontal">
                                <div className="card-stacked">
                                    <div className="card-content" style={{'paddingBottom': '0px', 'paddingTop': '0px'}}>
                                        <table>
                                            <thead>
                                            <tr>
                                                <th><img src={faker.image.avatar()} alt="avatar" className="circle"/>
                                                </th>
                                                <th><h5
                                                    style={{'marginBbottom': '0px'}}>{this.props.userProfile.name} {this.props.userProfile.surname}</h5>
                                                    <p className="teal-text text-darken-1">
                                                        <label>has been received from DB</label>
                                                    </p>
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td style={{'paddingTop': '5px', 'paddingBottom': '2px'}}>e-mail</td>
                                                <td style={{'paddingTop': '5px', 'paddingBottom': '2px'}}>
                                                    {this.props.userProfile.email ? this.props.userProfile.email : 'xxx@xxx.xx'}
                                                    <p><label>has been received from DB</label></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style={{'padding': '2px'}}>Birthday
                                                </td>
                                                <td style={{'padding': '2px'}}>2001
                                                    <p><label>has been fetched by the faker.js</label></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style={{'padding': '2px'}}>City
                                                </td>
                                                <td style={{'padding': '2px'}}>{faker.address.city()}
                                                    <p><label>... faker.js</label></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style={{'padding': '2px'}}>Company
                                                </td>
                                                <td style={{'padding': '2px'}}>{faker.company.companyName()}
                                                    <p><label>...faker.js</label></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style={{'padding': '2px'}}>WebCite
                                                </td>
                                                <td style={{'padding': '2px'}}>{faker.internet.domainName()}
                                                    <p><label>... faker.js</label></p>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div className="card-action">
                                        <a href="#">Send a message</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        );
    }
}

function mapStateToProps({auth}) {
    return {
        userProfile: {...auth.userProfile},
        usersList: auth.usersList,
    }
}

export default connect(
    mapStateToProps,
    {
        loadUserProfileAction,
        usersListAction,
    },
)(ShowProfile)
