import React, {Component} from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import {connect} from 'react-redux';
import * as actions from '../actions';

import '../styles/components/App.css';

import Header from './Header';
import Content from './Content';
import Signup from './signup/Signup'
import SuccessSignup from './signup/SuccessSignup'
import Login from './login/Login'
import Logout from './logout/Logout'
import EditProfile from './profile/EditProfile'
import EditProfileDone from './profile/EditProfileDone'
import ShowProfile from './profile/ShowProfile'
import Users from './users/Users'
import Friends from './friends/Friends'
import Post from './post/Post'
import PostDone from './post/PostDone'
import Tape from './tape/Tape'
// import InitializeFromStateForm from './test/InitializeFromStateForm'

class App extends Component {
    render() {
        // console.log('App render');
        return (
//TODO applying component={Signup} instead of component={props => <Signup {...props} />} leads to
//Warning: Failed prop type: Invalid prop `component` of type `object` supplied to `Route`, expected `function`.
//https://github.com/ReactTraining/react-router/issues/6420 account/register
            <BrowserRouter>
                <div id="shopify-section-header" className="shopify-section">
                    <div>
                        {/*<Signup/>*/}
                        <Header/>
                        <Route exact path="/" component={Content}/>
                        <Route path="/signup" component={props => <Signup {...props} />}/>
                        <Route path="/signupDone" component={props => <SuccessSignup {...props} />}/>
                        <Route path="/login" component={props => <Login {...props} />}/>
                        <Route path="/logout" component={props => <Logout {...props} />}/>
                        <Route path="/editProfile" component={props => <EditProfile {...props} />}/>
                        <Route path="/editProfileDone" component={props => <EditProfileDone {...props} />}/>
                        <Route path="/showProfile/:userId" component={props => <ShowProfile {...props} />}/>
                        <Route path="/users" component={props => <Users {...props} />}/>
                        <Route path="/friends" component={props => <Friends {...props} />}/>
                        <Route path="/post" component={props => <Post {...props} />}/>
                        <Route path="/postDone" component={props => <PostDone {...props} />}/>
                        <Route path="/tape" component={props => <Tape {...props} />}/>
                        {/*<Route path="/stateForm" component={props => <InitializeFromStateForm {...props} />}/>*/}

                    </div>
                </div>
            </BrowserRouter>
        );
    }
}

export default connect(null, actions)(App);
//export default App;