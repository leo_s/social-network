//TODO how to install to  /frontend 'npm install axios' by package.json
import axios from 'axios';
import {
    FETCH_USER,
    AUTO_LOGIN,
    LOGOUT,
    LOAD_USER_PROFILE,
} from './types';

export const fetchUser = () => async dispatch => {
    const res = await axios.get('/api/current_user');

    dispatch({type: FETCH_USER, payload: res.data});
};

export const createProfile = values => async dispatch => {
    const res = await axios.post('/api/surveys', values);

    dispatch({ type: FETCH_USER, payload: res.data });
};

export function addNumber(number = 15) {
    return{
        type: 'add_number',
        payload: number
    }
}

export function loginAction (values) {
    return function (dispatch) {
        return axios
            .post(`/api/login`, values)
            .then ((response) => {
                    dispatch({
                        type: FETCH_USER,
                        payload: response.data
                    })
                }
            )
    }
}

export function logoutAction() {
    return function (dispatch) {
        return axios
            .get(`/api/logout`)
            .then(() => {
                    dispatch({
                        type: LOGOUT,
                        // payload: response.data
                    })
                },
            )
    }
}

export function autoLogin() {
    return dispatch => {
        const isAuth = localStorage.getItem('isAuth')
        if(isAuth && isAuth.localeCompare('true') === 0) {
            dispatch({
                type: AUTO_LOGIN,
                payload: 'true'
            })
        }
    }

}

export function loadUserProfileAction(id) {
    // debugger
    return function (dispatch) {
        return axios
            .get(`/api/loadUserProfile/${id}`)
            // .get(`/api/loadUserProfile/1`)
            .then ((response) => {
                    dispatch({
                        type: LOAD_USER_PROFILE,
                        payload: response.data
                    })
                }
            )
    }
}