import axios from "axios";
import {CONF_FRIENDS_LIST, NON_CONF_FRIENDS_LIST, QUERY_TO_ME_LIST, USERS_LIST, USERS_LIST_FILTER} from "./types";

export function usersListAction() {
    return function (dispatch) {
        return axios
            .get(`/api/usersList`)
            .then ((response) => {
                    dispatch({
                        type: USERS_LIST,
                        payload: response.data
                    })
                }
            )
    }
}

export function usersListFilter(values) {
    return function (dispatch) {
        return axios
            .get(`/api/usersList/filter/${values.firstName}`)
            .then ((response) => {
                    dispatch({
                        type: USERS_LIST_FILTER,
                        payload: response.data,
                        previousFormValues: values
                    })
                }
            )
    }
}

export function sendFriendQueryAction(id, previousFormValues) {
    return function (dispatch) {
        const friends = JSON.stringify([id]);
        return axios
            .post(`/api/friendQuery`, {friends: friends})
            .then ((response) => {
                    dispatch(usersListFilter(previousFormValues))
                }
            )
    }
}

export function confirmedFriendsListAction() {
    return function (dispatch) {
        return axios
            .get(`/api/friendsList/1`)
            .then ((response) => {
                    dispatch({
                        type: CONF_FRIENDS_LIST,
                        payload: response.data
                    })
                }
            )
    }
}

export function nonConfirmedFriendsListAction() {
    return function (dispatch) {
        return axios
            .get(`/api/friendsList/0`)
            .then ((response) => {
                    dispatch({
                        type: NON_CONF_FRIENDS_LIST,
                        payload: response.data
                    })
                }
            )
    }
}

export function queryToMeListAction() {
    return function (dispatch) {
        return axios
            .get(`/api/queryToMeList/`)
            .then ((response) => {
                    dispatch({
                        type: QUERY_TO_ME_LIST,
                        payload: response.data
                    })
                }
            )
    }
}

export function sendFriendConfirmAction(id) {
    // debugger
    return function (dispatch) {
        const friends = JSON.stringify([id]);
        return axios
            .put(`/api/friendConfirm`, {friends: friends})
            .then ((response) => {
                    dispatch(queryToMeListAction())
                }
            )
    }
}

export function friendDeleteAction(id) {
    // debugger
    return function (dispatch) {
        return axios
            .put(`/api/friendDelete`, {friend: id})
            .then ((response) => {
                    dispatch(confirmedFriendsListAction())
                }
            )
    }
}

export function queryDeleteAction(id) {
    // debugger
    return function (dispatch) {
        return axios
            .put(`/api/friendDelete`, {friend: id})
            .then ((response) => {
                    dispatch(nonConfirmedFriendsListAction())
                }
            )
    }
}

