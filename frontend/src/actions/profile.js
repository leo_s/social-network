import axios from "axios";
import {CREATE_PROFILE, EDIT_PROFILE, LOAD_PROFILE} from "./types";


export function createProfileAction(values) {
    return function (dispatch) {
        return axios
            .post(`/api/createProfile`, values)
            .then((response) => {
                dispatch({
                    type: CREATE_PROFILE,
                    payload: response.data
                })
            })
    }
}

export function loadProfileAction() {
    return function (dispatch) {
        return axios
            .get(`/api/loadMyProfile`)
            .then ((response) => {
                    dispatch({
                        type: LOAD_PROFILE,
                        payload: response.data
                    })
                }
            )
    }
}

export function editProfileAction(values) {
    return function (dispatch) {
        return axios
            .put(`/api/editProfile`, values)
            .then((response) => {
                dispatch({
                    type: EDIT_PROFILE,
                    payload: response.data
                })
            })
    }
}


