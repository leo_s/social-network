import axios from "axios";
import {CREATE_POST, POST_LIST} from "./types";

export function createPostAction(values) {
    return function (dispatch) {
        return axios
            .post(`/api/createPost`, values)
            .then((response) => {
                dispatch({
                    type: CREATE_POST,
                    payload: response.data
                })
            })
    }
}

export function postsListAction(page) {
    // debugger
    return function (dispatch) {
        return axios
            .get(`/api/showTape/${page}`)
            .then((response) => {
                dispatch({
                    type: POST_LIST,
                    payload: response.data
                })
            })
    }
}
