//import 'materialize-css/dist/css/materialize.min.css';
//TODO eslint-plugin-react is employing ~/node_modules instead of ~/tumbtack/social-network/node-modules
//TODO if delet ~/node_modules it dosn't work
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import reduxThunk from 'redux-thunk';

import App from './components/App';
import reducers from './reducers';

const composeEnhancers =
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
            // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
        }) : compose;

const store = createStore(reducers, {}, composeEnhancers(applyMiddleware(reduxThunk)));

render(
    <Provider store={store}><App /></Provider>,
    document.getElementById('app')
);
