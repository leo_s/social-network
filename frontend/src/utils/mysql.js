insert into social_network.users (name, surname, email, password, created_at, updated_at) values ('Aaa', 'Zzz', '111@gmail.com', '$2b$08$8G98EViJzgIDhSboQ6FOR.TuXWEEN.SCmpD33xonksa7beg3v3CQi', '2018-12-29 13:58:09', '2018-12-29 13:58:09');
insert into social_network.users (name, surname, email, password, created_at, updated_at) values ('Bbb', 'Zzz', '222@gmail.com', '$2b$08$8G98EViJzgIDhSboQ6FOR.TuXWEEN.SCmpD33xonksa7beg3v3CQi', '2018-12-29 13:58:09', '2018-12-29 13:58:09');
insert into social_network.users (name, surname, email, password, created_at, updated_at) values ('Ccc', 'Zzz', '333@gmail.com', '$2b$08$8G98EViJzgIDhSboQ6FOR.TuXWEEN.SCmpD33xonksa7beg3v3CQi', '2018-12-29 13:58:09', '2018-12-29 13:58:09');
insert into social_network.users (name, surname, email, password, created_at, updated_at) values ('Ddd', 'Zzz', '444@gmail.com', '$2b$08$8G98EViJzgIDhSboQ6FOR.TuXWEEN.SCmpD33xonksa7beg3v3CQi', '2018-12-29 13:58:09', '2018-12-29 13:58:09');
insert into social_network.users (name, surname, email, password, created_at, updated_at) values ('Eee', 'Zzz', '555@gmail.com', '$2b$08$8G98EViJzgIDhSboQ6FOR.TuXWEEN.SCmpD33xonksa7beg3v3CQi', '2018-12-29 13:58:09', '2018-12-29 13:58:09');
insert into social_network.users (name, surname, email, password, created_at, updated_at) values ('Fff', 'Zzz', '666@gmail.com', '$2b$08$8G98EViJzgIDhSboQ6FOR.TuXWEEN.SCmpD33xonksa7beg3v3CQi', '2018-12-29 13:58:09', '2018-12-29 13:58:09');
insert into social_network.users (name, surname, email, password, created_at, updated_at) values ('Ggg', 'Zzz', '777@gmail.com', '$2b$08$8G98EViJzgIDhSboQ6FOR.TuXWEEN.SCmpD33xonksa7beg3v3CQi', '2018-12-29 13:58:09', '2018-12-29 13:58:09');
insert into social_network.users (name, surname, email, password, created_at, updated_at) values ('Hhh', 'Zzz', '888@gmail.com', '$2b$08$8G98EViJzgIDhSboQ6FOR.TuXWEEN.SCmpD33xonksa7beg3v3CQi', '2018-12-29 13:58:09', '2018-12-29 13:58:09');
insert into social_network.users (name, surname, email, password, created_at, updated_at) values ('Iii', 'Zzz', '999@gmail.com', '$2b$08$8G98EViJzgIDhSboQ6FOR.TuXWEEN.SCmpD33xonksa7beg3v3CQi', '2018-12-29 13:58:09', '2018-12-29 13:58:09');
insert into social_network.users (name, surname, email, password, created_at, updated_at) values ('Jjj', 'Zzz', '000@gmail.com', '$2b$08$8G98EViJzgIDhSboQ6FOR.TuXWEEN.SCmpD33xonksa7beg3v3CQi', '2018-12-29 13:58:09', '2018-12-29 13:58:09');
insert into social_network.users (name, surname, email, password, created_at, updated_at) values ('Zzz', 'Aaa', 'a111@gmail.com', '$2b$08$8G98EViJzgIDhSboQ6FOR.TuXWEEN.SCmpD33xonksa7beg3v3CQi', '2018-12-29 13:58:09', '2018-12-29 13:58:09');
insert into social_network.users (name, surname, email, password, created_at, updated_at) values ('Zzz', 'Bbb', 'b222@gmail.com', '$2b$08$8G98EViJzgIDhSboQ6FOR.TuXWEEN.SCmpD33xonksa7beg3v3CQi', '2018-12-29 13:58:09', '2018-12-29 13:58:09');



INSERT INTO social_network.friends (confirmed, created_at, updated_at, user_id, master_id) values (1, '2018-12-29 13:58:09', '2018-12-29 13:58:09', 2, 1);
INSERT INTO social_network.friends (confirmed, created_at, updated_at, user_id, master_id) values (1, '2018-12-29 13:58:09', '2018-12-29 13:58:09', 3, 1);
INSERT INTO social_network.friends (confirmed, created_at, updated_at, user_id, master_id) values (1, '2018-12-29 13:58:09', '2018-12-29 13:58:09', 4, 1);
INSERT INTO social_network.friends (confirmed, created_at, updated_at, user_id, master_id) values (1, '2018-12-29 13:58:09', '2018-12-29 13:58:09', 5, 1);
INSERT INTO social_network.friends (confirmed, created_at, updated_at, user_id, master_id) values (1, '2018-12-29 13:58:09', '2018-12-29 13:58:09', 1, 2);
INSERT INTO social_network.friends (confirmed, created_at, updated_at, user_id, master_id) values (0, '2018-12-29 13:58:09', '2018-12-29 13:58:09', 3, 2);
INSERT INTO social_network.friends (confirmed, created_at, updated_at, user_id, master_id) values (0, '2018-12-29 13:58:09', '2018-12-29 13:58:09', 4, 2);
INSERT INTO social_network.friends (confirmed, created_at, updated_at, user_id, master_id) values (0, '2018-12-29 13:58:09', '2018-12-29 13:58:09', 5, 2);
INSERT INTO social_network.friends (confirmed, created_at, updated_at, user_id, master_id) values (1, '2018-12-29 13:58:09', '2018-12-29 13:58:09', 1, 3);
INSERT INTO social_network.friends (confirmed, created_at, updated_at, user_id, master_id) values (0, '2018-12-29 13:58:09', '2018-12-29 13:58:09', 4, 3);
INSERT INTO social_network.friends (confirmed, created_at, updated_at, user_id, master_id) values (0, '2018-12-29 13:58:09', '2018-12-29 13:58:09', 5, 3);
INSERT INTO social_network.friends (confirmed, created_at, updated_at, user_id, master_id) values (1, '2018-12-29 13:58:09', '2018-12-29 13:58:09', 1, 4);
INSERT INTO social_network.friends (confirmed, created_at, updated_at, user_id, master_id) values (0, '2018-12-29 13:58:09', '2018-12-29 13:58:09', 5, 4);
INSERT INTO social_network.friends (confirmed, created_at, updated_at, user_id, master_id) values (1, '2018-12-29 13:58:09', '2018-12-29 13:58:09', 1, 5);

INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('abcdefghijklmnopqrstuvwxyz 111@gmail.com', '2018-01-01 13:00:00', '2018-01-01 13:00:00', 1);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('bcdefghijklmnopqrstuvwxyz 111@gmail.com', '2018-02-01 13:00:00', '2018-02-01 13:00:00', 1);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('cdefghijklmnopqrstuvwxyz 111@gmail.com', '2018-03-01 13:00:00', '2018-03-01 13:00:00', 1);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('defghijklmnopqrstuvwxyz 111@gmail.com', '2018-04-01 13:00:00', '2018-04-01 13:00:00', 1);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('efghijklmnopqrstuvwxyz 111@gmail.com', '2018-05-01 13:00:00', '2018-05-01 13:00:00', 1);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('fghijklmnopqrstuvwxyz 111@gmail.com', '2018-06-01 13:00:00', '2018-06-01 13:00:00', 1);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('ghijklmnopqrstuvwxyz 111@gmail.com', '2018-06-01 14:00:00', '2018-06-01 14:00:00', 1);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('hijklmnopqrstuvwxyz 111@gmail.com', '2018-06-01 15:00:00', '2018-06-01 15:00:00', 1);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('ijklmnopqrstuvwxyz 111@gmail.com', '2018-06-01 16:00:00', '2018-06-01 16:00:00', 1);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('jklmnopqrstuvwxyz 111@gmail.com', '2018-06-01 17:00:00', '2018-06-01 17:00:00', 1);

INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('abcdefghijklmnopqrstuvwxyz 222@gmail.com', '2018-01-01 13:00:00', '2018-01-01 13:00:00', 2);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('bcdefghijklmnopqrstuvwxyz 222@gmail.com', '2018-02-01 13:00:00', '2018-02-01 13:00:00', 2);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('cdefghijklmnopqrstuvwxyz 222@gmail.com', '2018-03-01 13:00:00', '2018-03-01 13:00:00', 2);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('defghijklmnopqrstuvwxyz 222@gmail.com', '2018-04-01 13:00:00', '2018-04-01 13:00:00', 2);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('efghijklmnopqrstuvwxyz 222@gmail.com', '2018-05-01 13:00:00', '2018-05-01 13:00:00', 2);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('fghijklmnopqrstuvwxyz 222@gmail.com', '2018-06-01 13:00:00', '2018-06-01 13:00:00', 2);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('ghijklmnopqrstuvwxyz 222@gmail.com', '2018-06-01 14:00:00', '2018-06-01 14:00:00', 2);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('hijklmnopqrstuvwxyz 222@gmail.com', '2018-06-01 15:00:00', '2018-06-01 15:00:00', 2);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('ijklmnopqrstuvwxyz 222@gmail.com', '2018-06-01 16:00:00', '2018-06-01 16:00:00', 2);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('jklmnopqrstuvwxyz 222@gmail.com', '2018-06-01 17:00:00', '2018-06-01 17:00:00', 2);

INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('abcdefghijklmnopqrstuvwxyz 333@gmail.com', '2018-01-01 13:00:00', '2018-01-01 13:00:00', 3);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('bcdefghijklmnopqrstuvwxyz 333@gmail.com', '2018-02-01 13:00:00', '2018-02-01 13:00:00', 3);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('cdefghijklmnopqrstuvwxyz 333@gmail.com', '2018-03-01 13:00:00', '2018-03-01 13:00:00', 3);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('defghijklmnopqrstuvwxyz 333@gmail.com', '2018-04-01 13:00:00', '2018-04-01 13:00:00', 3);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('efghijklmnopqrstuvwxyz 333@gmail.com', '2018-05-01 13:00:00', '2018-05-01 13:00:00', 3);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('fghijklmnopqrstuvwxyz 333@gmail.com', '2018-06-01 13:00:00', '2018-06-01 13:00:00', 3);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('ghijklmnopqrstuvwxyz 333@gmail.com', '2018-06-01 14:00:00', '2018-06-01 14:00:00', 3);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('hijklmnopqrstuvwxyz 333@gmail.com', '2018-06-01 15:00:00', '2018-06-01 15:00:00', 3);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('ijklmnopqrstuvwxyz 333@gmail.com', '2018-06-01 16:00:00', '2018-06-01 16:00:00', 3);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('jklmnopqrstuvwxyz 333@gmail.com', '2018-06-01 17:00:00', '2018-06-01 17:00:00', 3);

INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('abcdefghijklmnopqrstuvwxyz 444@gmail.com', '2018-01-01 13:00:00', '2018-01-01 13:00:00', 4);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('bcdefghijklmnopqrstuvwxyz 444@gmail.com', '2018-02-01 13:00:00', '2018-02-01 13:00:00', 4);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('cdefghijklmnopqrstuvwxyz 444@gmail.com', '2018-03-01 13:00:00', '2018-03-01 13:00:00', 4);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('defghijklmnopqrstuvwxyz 444@gmail.com', '2018-04-01 13:00:00', '2018-04-01 13:00:00', 4);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('efghijklmnopqrstuvwxyz 444@gmail.com', '2018-05-01 13:00:00', '2018-05-01 13:00:00', 4);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('fghijklmnopqrstuvwxyz 444@gmail.com', '2018-06-01 13:00:00', '2018-06-01 13:00:00', 4);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('ghijklmnopqrstuvwxyz 444@gmail.com', '2018-06-01 14:00:00', '2018-06-01 14:00:00', 4);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('hijklmnopqrstuvwxyz 444@gmail.com', '2018-06-01 15:00:00', '2018-06-01 15:00:00', 4);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('ijklmnopqrstuvwxyz 444@gmail.com', '2018-06-01 16:00:00', '2018-06-01 16:00:00', 4);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('jklmnopqrstuvwxyz 444@gmail.com', '2018-06-01 17:00:00', '2018-06-01 17:00:00', 4);

INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('abcdefghijklmnopqrstuvwxyz 555@gmail.com', '2018-01-01 13:00:00', '2018-01-01 13:00:00', 5);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('bcdefghijklmnopqrstuvwxyz 555@gmail.com', '2018-02-01 13:00:00', '2018-02-01 13:00:00', 5);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('cdefghijklmnopqrstuvwxyz 555@gmail.com', '2018-03-01 13:00:00', '2018-03-01 13:00:00', 5);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('defghijklmnopqrstuvwxyz 555@gmail.com', '2018-04-01 13:00:00', '2018-04-01 13:00:00', 5);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('efghijklmnopqrstuvwxyz 555@gmail.com', '2018-05-01 13:00:00', '2018-05-01 13:00:00', 5);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('fghijklmnopqrstuvwxyz 555@gmail.com', '2018-06-01 13:00:00', '2018-06-01 13:00:00', 5);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('ghijklmnopqrstuvwxyz 555@gmail.com', '2018-06-01 14:00:00', '2018-06-01 14:00:00', 5);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('hijklmnopqrstuvwxyz 555@gmail.com', '2018-06-01 15:00:00', '2018-06-01 15:00:00', 5);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('ijklmnopqrstuvwxyz 555@gmail.com', '2018-06-01 16:00:00', '2018-06-01 16:00:00', 5);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('jklmnopqrstuvwxyz 555@gmail.com', '2018-06-01 17:00:00', '2018-06-01 17:00:00', 5);

INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('abcdefghijklmnopqrstuvwxyz 666@gmail.com', '2018-01-01 13:00:00', '2018-01-01 13:00:00', 6);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('bcdefghijklmnopqrstuvwxyz 666@gmail.com', '2018-02-01 13:00:00', '2018-02-01 13:00:00', 6);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('cdefghijklmnopqrstuvwxyz 666@gmail.com', '2018-03-01 13:00:00', '2018-03-01 13:00:00', 6);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('defghijklmnopqrstuvwxyz 666@gmail.com', '2018-04-01 13:00:00', '2018-04-01 13:00:00', 6);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('efghijklmnopqrstuvwxyz 666@gmail.com', '2018-05-01 13:00:00', '2018-05-01 13:00:00', 6);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('fghijklmnopqrstuvwxyz 666@gmail.com', '2018-06-01 13:00:00', '2018-06-01 13:00:00', 6);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('ghijklmnopqrstuvwxyz 666@gmail.com', '2018-06-01 14:00:00', '2018-06-01 14:00:00', 6);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('hijklmnopqrstuvwxyz 666@gmail.com', '2018-06-01 15:00:00', '2018-06-01 15:00:00', 6);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('ijklmnopqrstuvwxyz 666@gmail.com', '2018-06-01 16:00:00', '2018-06-01 16:00:00', 6);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('jklmnopqrstuvwxyz 666@gmail.com', '2018-06-01 17:00:00', '2018-06-01 17:00:00', 6);

INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('abcdefghijklmnopqrstuvwxyz 777@gmail.com', '2018-01-01 13:00:00', '2018-01-01 13:00:00', 7);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('bcdefghijklmnopqrstuvwxyz 777@gmail.com', '2018-02-01 13:00:00', '2018-02-01 13:00:00', 7);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('cdefghijklmnopqrstuvwxyz 777@gmail.com', '2018-03-01 13:00:00', '2018-03-01 13:00:00', 7);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('defghijklmnopqrstuvwxyz 777@gmail.com', '2018-04-01 13:00:00', '2018-04-01 13:00:00', 7);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('efghijklmnopqrstuvwxyz 777@gmail.com', '2018-05-01 13:00:00', '2018-05-01 13:00:00', 7);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('fghijklmnopqrstuvwxyz 777@gmail.com', '2018-06-01 13:00:00', '2018-06-01 13:00:00', 7);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('ghijklmnopqrstuvwxyz 777@gmail.com', '2018-06-01 14:00:00', '2018-06-01 14:00:00', 7);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('hijklmnopqrstuvwxyz 777@gmail.com', '2018-06-01 15:00:00', '2018-06-01 15:00:00', 7);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('ijklmnopqrstuvwxyz 777@gmail.com', '2018-06-01 16:00:00', '2018-06-01 16:00:00', 7);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('jklmnopqrstuvwxyz 777@gmail.com', '2018-06-01 17:00:00', '2018-06-01 17:00:00', 7);

INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('abcdefghijklmnopqrstuvwxyz 888@gmail.com', '2018-01-01 13:00:00', '2018-01-01 13:00:00', 8);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('bcdefghijklmnopqrstuvwxyz 888@gmail.com', '2018-02-01 13:00:00', '2018-02-01 13:00:00', 8);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('cdefghijklmnopqrstuvwxyz 888@gmail.com', '2018-03-01 13:00:00', '2018-03-01 13:00:00', 8);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('defghijklmnopqrstuvwxyz 888@gmail.com', '2018-04-01 13:00:00', '2018-04-01 13:00:00', 8);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('efghijklmnopqrstuvwxyz 888@gmail.com', '2018-05-01 13:00:00', '2018-05-01 13:00:00', 8);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('fghijklmnopqrstuvwxyz 888@gmail.com', '2018-06-01 13:00:00', '2018-06-01 13:00:00', 8);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('ghijklmnopqrstuvwxyz 888@gmail.com', '2018-06-01 14:00:00', '2018-06-01 14:00:00', 8);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('hijklmnopqrstuvwxyz 888@gmail.com', '2018-06-01 15:00:00', '2018-06-01 15:00:00', 8);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('ijklmnopqrstuvwxyz 888@gmail.com', '2018-06-01 16:00:00', '2018-06-01 16:00:00', 8);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('jklmnopqrstuvwxyz 888@gmail.com', '2018-06-01 17:00:00', '2018-06-01 17:00:00', 8);

INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('abcdefghijklmnopqrstuvwxyz 999@gmail.com', '2018-01-01 13:00:00', '2018-01-01 13:00:00', 9);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('bcdefghijklmnopqrstuvwxyz 999@gmail.com', '2018-02-01 13:00:00', '2018-02-01 13:00:00', 9);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('cdefghijklmnopqrstuvwxyz 999@gmail.com', '2018-03-01 13:00:00', '2018-03-01 13:00:00', 9);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('defghijklmnopqrstuvwxyz 999@gmail.com', '2018-04-01 13:00:00', '2018-04-01 13:00:00', 9);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('efghijklmnopqrstuvwxyz 999@gmail.com', '2018-05-01 13:00:00', '2018-05-01 13:00:00', 9);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('fghijklmnopqrstuvwxyz 999@gmail.com', '2018-06-01 13:00:00', '2018-06-01 13:00:00', 9);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('ghijklmnopqrstuvwxyz 999@gmail.com', '2018-06-01 14:00:00', '2018-06-01 14:00:00', 9);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('hijklmnopqrstuvwxyz 999@gmail.com', '2018-06-01 15:00:00', '2018-06-01 15:00:00', 9);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('ijklmnopqrstuvwxyz 999@gmail.com', '2018-06-01 16:00:00', '2018-06-01 16:00:00', 9);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('jklmnopqrstuvwxyz 999@gmail.com', '2018-06-01 17:00:00', '2018-06-01 17:00:00', 9);

INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('abcdefghijklmnopqrstuvwxyz 000@gmail.com', '2018-01-01 13:00:00', '2018-01-01 13:00:00', 10);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('bcdefghijklmnopqrstuvwxyz 000@gmail.com', '2018-02-01 13:00:00', '2018-02-01 13:00:00', 10);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('cdefghijklmnopqrstuvwxyz 000@gmail.com', '2018-03-01 13:00:00', '2018-03-01 13:00:00', 10);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('defghijklmnopqrstuvwxyz 000@gmail.com', '2018-04-01 13:00:00', '2018-04-01 13:00:00', 10);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('efghijklmnopqrstuvwxyz 000@gmail.com', '2018-05-01 13:00:00', '2018-05-01 13:00:00', 10);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('fghijklmnopqrstuvwxyz 000@gmail.com', '2018-06-01 13:00:00', '2018-06-01 13:00:00', 10);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('ghijklmnopqrstuvwxyz 000@gmail.com', '2018-06-01 14:00:00', '2018-06-01 14:00:00', 10);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('hijklmnopqrstuvwxyz 000@gmail.com', '2018-06-01 15:00:00', '2018-06-01 15:00:00', 10);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('ijklmnopqrstuvwxyz 000@gmail.com', '2018-06-01 16:00:00', '2018-06-01 16:00:00', 10);
INSERT INTO social_network.posts (post, created_at, updated_at, user_id) values ('jklmnopqrstuvwxyz 000@gmail.com', '2018-06-01 17:00:00', '2018-06-01 17:00:00', 10);





delete from social_network.users

