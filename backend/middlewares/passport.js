const passport = require('koa-passport');
const LocalStrategy = require('passport-local');
const db = require('../models');
const UtilService = require('../../services/util.service');
//const bcrypt = require('bcrypt');

const options = {
    usernameField: 'email',
    passwordField: 'password'
};

passport.use('local', new LocalStrategy(options, async (email, password, done) => {
    const user = await db.User.findOne({where: {email: email}});
    if(user !== null){
        const matched = await UtilService.comparedPassword(password, user.password);
        //pass model to passport
        //https://medium.freecodecamp.org/learn-how-to-handle-authentication-with-node-using-passport-js-4a56ed18e81e
        //http://www.passportjs.org/docs/authenticate/
        // Fetch real user data from db and check if passed credentials match
        if (matched) {
            /*TODO if(bcrypt.compareSync(password, user.password)) {*/
            //TODO see Password Hashing https://mherman.org/blog/user-authentication-with-passport-and-koa/
            //if(bcrypt.compareSync(password, user.password)) {
            //if (email === 'test@example.com' && password === 'password') {
            return done(null, {
                email,
                //password,
                id: user.id
            });
        }
    }

    return done(null, false);
}));

passport.serializeUser((user, done) => {
    done(null, user.id);
});

passport.deserializeUser(async (userId, done) => {
    // Fetch real user data from db
    //TODO see http://www.passportjs.org/docs/configure/#sessions
    const user = await db.User.findOne({where: {id: userId}});
    done(null, {
        email: user.email,
        name: user.name,
        surname: user.surname,
        id: user.id
    });
});

module.exports = passport;