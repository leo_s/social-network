module.exports = {
  development: {
    db: {
      name: 'social_network',
      username: 'sn_admin',
      password: '1qaz@WSX',
      host: 'localhost',
      port: 3306,
      dialect: 'mysql',
      logging: true
    }
  },
  test: {
    db: {
      name: 'sn_test',
      username: 'sn_admin',
      password: '1qaz@WSX',
      host: 'localhost',
      port: 3306,
      dialect: 'mysql',
      logging: false
    }
  }
};