const app = require('../../app');

/*global before*/
before(async function() {
  this.timeout(10000);
  this.app = await app;
});