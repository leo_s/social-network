const chai = require('chai');
const chaiHttp = require('chai-http');
const models = require('../../models');

const {expect} = chai;
chai.use(chaiHttp);

/*global describe before it after*/
describe('Posts test', function () {
    after(async function () {
        await models.sequelize.sync({ force: true });
    });

    describe('Account creation', function () {
        const accounts = [
            {'password': '1qaz!QAZ', 'name': 'Gek', 'surname': 'Black', 'email': '111@gmail.com',},
            {'password': '1qaz!QAZ', 'name': 'Billy', 'surname': 'White', 'email': '222@gmail.com',},
            {'password': '1qaz!QAZ', 'name': 'Gek', 'surname': 'Grey', 'email': '333@gmail.com',},
            {'password': '1qaz!QAZ', 'name': 'Billy', 'surname': 'Grey', 'email': '444@gmail.com',},
            {'password': '1qaz!QAZ', 'name': 'Петя', 'surname': 'Пончик', 'email': '555@gmail.com',}
        ];

        accounts.forEach(function (account) {
            describe('Create accounts', function () {
                before(async function () {
                    this.response = await chai.request(this.app)
                        .post(`/api/createProfile`)
                        .send({
                            password: account.password,
                            name: account.name,
                            surname: account.surname,
                            email: account.email,
                        });
                });

                it('should respond with 200', function () {
                    expect(this.response).to.have.status(200);
                });
            });
        });

    });

    describe('Friend fastening', function () {

        describe('POST. User 1 sends one friend query for the persons 2, 3, 4', function () {
            before(async function () {
                const agent = chai.request.agent(this.app);
                await agent.post('/api/login').send({
                    email: '111@gmail.com',
                    password: '1qaz!QAZ',
                });
                const friends = JSON.stringify(['2', '3', '4']);
                this.response = await agent
                    .post('/api/friendQuery').send({
                        friends: friends,
                    });
            });

            it('should respond with 200', function () {
                expect(this.response).to.have.status(200);
            });
        });

        describe('POST. User 2 sends one friend query for the persons 1, 3, 4', function () {
            before(async function () {
                const agent = chai.request.agent(this.app);
                await agent.post('/api/login').send({
                    email: '222@gmail.com',
                    password: '1qaz!QAZ',
                });
                const friends = JSON.stringify(['1', '3', '4']);
                this.response = await agent
                    .post('/api/friendQuery').send({
                        friends: friends,
                    });
            });

            it('should respond with 200', function () {
                expect(this.response).to.have.status(200);
            });
        });

        describe('POST. User 3 sends one friend confirm for the persons 1, 2', function () {
            before(async function () {
                const friends = JSON.stringify(['1','2']);
                const agent = chai.request.agent(this.app);
                await agent.post('/api/login').send({
                    email: '333@gmail.com',
                    password: '1qaz!QAZ',
                });
                this.response = await agent
                    .put('/api/friendConfirm').send({
                        friends: friends,
                    });
            });

            it('should respond with 200', function () {
                expect(this.response).to.have.status(200);
            });
        });

    });

    describe('Posts record check', function () {
        describe('User 1 sends one post', function () {
            before(async function () {
                const agent = chai.request.agent(this.app);
                await agent.post('/api/login').send({
                    email: '111@gmail.com',
                    password: '1qaz!QAZ',
                });
                const post = '0 post';
                this.response = await agent
                    .post('/api/createPost').send({
                        post: post,
                    });
            });

            it('should respond with 200', function () {
                expect(this.response).to.have.status(200);
            });
            it('should respond with message', function () {
                expect(this.response.text).to.equals('Your post is written.');
            });
        });
    });

    describe('Multiple posts record', function () {

        const posts = [
            {
                'user_id': 1,
                'text': '1 post user1',
                'created_at': '2018-11-01 12:46:43',
                'updated_at': '2018-11-01 12:46:43'
            },
            {
                'user_id': 1,
                'text': '2 post user1',
                'created_at': '2018-11-02 12:46:43',
                'updated_at': '2018-11-02 12:46:43'
            },
            {
                'user_id': 1,
                'text': '3 post user1',
                'created_at': '2018-11-03 12:46:43',
                'updated_at': '2018-11-03 12:46:43'
            },
            {
                'user_id': 1,
                'text': '4 post user1',
                'created_at': '2018-11-04 12:46:43',
                'updated_at': '2018-11-04 12:46:43'
            },
            {
                'user_id': 1,
                'text': '5 post user1',
                'created_at': '2018-11-05 12:46:43',
                'updated_at': '2018-11-05 12:46:43'
            },
            {
                'user_id': 1,
                'text': '6 post user1',
                'created_at': '2018-11-06 12:46:43',
                'updated_at': '2018-11-06 12:46:43'
            },
            {
                'user_id': 1,
                'text': '7 post user1',
                'created_at': '2018-11-07 12:46:43',
                'updated_at': '2018-11-07 12:46:43'
            },
            {
                'user_id': 1,
                'text': '8 post user1',
                'created_at': '2018-11-08 12:46:43',
                'updated_at': '2018-11-08 12:46:43'
            },
            {
                'user_id': 1,
                'text': '9 post user1',
                'created_at': '2018-11-09 12:46:43',
                'updated_at': '2018-11-09 12:46:43'
            },
            {
                'user_id': 1,
                'text': '10 post user1',
                'created_at': '2018-11-10 12:46:43',
                'updated_at': '2018-11-10 12:46:43'
            },
            {
                'user_id': 1,
                'text': '11 post user1',
                'created_at': '2018-11-11 12:46:43',
                'updated_at': '2018-11-11 12:46:43'
            },
            {
                'user_id': 1,
                'text': '12 post user1',
                'created_at': '2018-11-12 12:46:43',
                'updated_at': '2018-11-12 12:46:43'
            },
            {
                'user_id': 1,
                'text': '13 post user1',
                'created_at': '2018-11-13 12:46:43',
                'updated_at': '2018-11-13 12:46:43'
            },
            {
                'user_id': 1,
                'text': '14 post user1',
                'created_at': '2018-11-14 12:46:43',
                'updated_at': '2018-11-14 12:46:43'
            },
            {
                'user_id': 1,
                'text': '15 post user1',
                'created_at': '2018-11-15 12:46:43',
                'updated_at': '2018-11-15 12:46:43'
            },
            {
                'user_id': 1,
                'text': '16 post user1',
                'created_at': '2018-11-16 12:46:43',
                'updated_at': '2018-11-16 12:46:43'
            },

            {
                'user_id': 2,
                'text': '1 post user2',
                'created_at': '2018-11-01 12:46:43',
                'updated_at': '2018-11-01 12:46:43'
            },
            {
                'user_id': 2,
                'text': '2 post user2',
                'created_at': '2018-11-02 12:46:43',
                'updated_at': '2018-11-02 12:46:43'
            },
            {
                'user_id': 2,
                'text': '3 post user2',
                'created_at': '2018-11-03 12:46:43',
                'updated_at': '2018-11-03 12:46:43'
            },
            {
                'user_id': 2,
                'text': '4 post user2',
                'created_at': '2018-11-04 12:46:43',
                'updated_at': '2018-11-04 12:46:43'
            },
            {
                'user_id': 2,
                'text': '5 post user2',
                'created_at': '2018-11-05 12:46:43',
                'updated_at': '2018-11-05 12:46:43'
            },
            {
                'user_id': 2,
                'text': '6 post user2',
                'created_at': '2018-11-06 12:46:43',
                'updated_at': '2018-11-06 12:46:43'
            },
            {
                'user_id': 2,
                'text': '7 post user2',
                'created_at': '2018-11-07 12:46:43',
                'updated_at': '2018-11-07 12:46:43'
            },
            {
                'user_id': 2,
                'text': '8 post user2',
                'created_at': '2018-11-08 12:46:43',
                'updated_at': '2018-11-08 12:46:43'
            },
            {
                'user_id': 2,
                'text': '9 post user2',
                'created_at': '2018-11-09 12:46:43',
                'updated_at': '2018-11-09 12:46:43'
            },
            {
                'user_id': 2,
                'text': '10 post user2',
                'created_at': '2018-11-10 12:46:43',
                'updated_at': '2018-11-10 12:46:43'
            },
            {
                'user_id': 2,
                'text': '11 post user2',
                'created_at': '2018-11-11 12:46:43',
                'updated_at': '2018-11-11 12:46:43'
            },
            {
                'user_id': 2,
                'text': '12 post user2',
                'created_at': '2018-11-12 12:46:43',
                'updated_at': '2018-11-12 12:46:43'
            },
            {
                'user_id': 2,
                'text': '13 post user2',
                'created_at': '2018-11-13 12:46:43',
                'updated_at': '2018-11-13 12:46:43'
            },
            {
                'user_id': 2,
                'text': '14 post user2',
                'created_at': '2018-11-14 12:46:43',
                'updated_at': '2018-11-14 12:46:43'
            },
            {
                'user_id': 2,
                'text': '15 post user2',
                'created_at': '2018-11-15 12:46:43',
                'updated_at': '2018-11-15 12:46:43'
            },
            {
                'user_id': 2,
                'text': '16 post user2',
                'created_at': '2018-11-16 12:46:43',
                'updated_at': '2018-11-16 12:46:43'
            },

            {
                'user_id': 3,
                'text': '1 post user3',
                'created_at': '2018-11-01 12:46:43',
                'updated_at': '2018-11-01 12:46:43'
            },
            {
                'user_id': 3,
                'text': '2 post user3',
                'created_at': '2018-11-02 12:46:43',
                'updated_at': '2018-11-02 12:46:43'
            },
            {
                'user_id': 3,
                'text': '3 post user3',
                'created_at': '2018-11-03 12:46:43',
                'updated_at': '2018-11-03 12:46:43'
            },
            {
                'user_id': 3,
                'text': '4 post user3',
                'created_at': '2018-11-04 12:46:43',
                'updated_at': '2018-11-04 12:46:43'
            },
            {
                'user_id': 3,
                'text': '5 post user3',
                'created_at': '2018-11-05 12:46:43',
                'updated_at': '2018-11-05 12:46:43'
            },
            {
                'user_id': 3,
                'text': '6 post user3',
                'created_at': '2018-11-06 12:46:43',
                'updated_at': '2018-11-06 12:46:43'
            },
            {
                'user_id': 3,
                'text': '7 post user3',
                'created_at': '2018-11-07 12:46:43',
                'updated_at': '2018-11-07 12:46:43'
            },
            {
                'user_id': 3,
                'text': '8 post user3',
                'created_at': '2018-11-08 12:46:43',
                'updated_at': '2018-11-08 12:46:43'
            },
            {
                'user_id': 3,
                'text': '9 post user3',
                'created_at': '2018-11-09 12:46:43',
                'updated_at': '2018-11-09 12:46:43'
            },
            {
                'user_id': 3,
                'text': '10 post user3',
                'created_at': '2018-11-10 12:46:43',
                'updated_at': '2018-11-10 12:46:43'
            },
            {
                'user_id': 3,
                'text': '11 post user3',
                'created_at': '2018-11-11 12:46:43',
                'updated_at': '2018-11-11 12:46:43'
            },
            {
                'user_id': 3,
                'text': '12 post user3',
                'created_at': '2018-11-12 12:46:43',
                'updated_at': '2018-11-12 12:46:43'
            },
            {
                'user_id': 3,
                'text': '13 post user3',
                'created_at': '2018-11-13 12:46:43',
                'updated_at': '2018-11-13 12:46:43'
            },
            {
                'user_id': 3,
                'text': '14 post user3',
                'created_at': '2018-11-14 12:46:43',
                'updated_at': '2018-11-14 12:46:43'
            },
            {
                'user_id': 3,
                'text': '15 post user3',
                'created_at': '2018-11-15 12:46:43',
                'updated_at': '2018-11-15 12:46:43'
            },
            {
                'user_id': 3,
                'text': '16 post user3',
                'created_at': '2018-11-16 12:46:43',
                'updated_at': '2018-11-16 12:46:43'
            },
            {
                'user_id': 4,
                'text': '1 post user4',
                'created_at': '2018-11-03 12:46:43',
                'updated_at': '2018-11-03 12:46:43'
            },

        ];
        posts.forEach(function (post) {
            describe('Direct posts record', function () {
                it('wri', async function () {
                    await models.Post.create({
                        post: post.text,
                        user_id: post.user_id,
                        created_at: post.created_at,
                    })
                });
            });
        });
    });

    describe('Show Tape', function () {
        before(async function () {
            const agent = chai.request.agent(this.app);
            await agent.post('/api/login').send({
                email: '333@gmail.com',
                password: '1qaz!QAZ',
            });
            this.response = await agent
                .get(`/api/showTape/2`)
                .send();
        });
        it('should respond with 200', function () {
            expect(this.response).to.have.status(200);
        });
        it('should respond with message', function () {
            const input = JSON.parse(this.response.text);
            expect(input).to.deep.equals(
                {
                    "count": 33,
                    "rows": [
                        {
                            "created_at": "2018-11-06T06:46:43.000Z",
                            "name": "Billy",
                            "post": "6 post user2",
                            "surname": "White",
                            "user_id": 2
                        },
                        {
                            "created_at": "2018-11-06T06:46:43.000Z",
                            "name": "Gek",
                            "post": "6 post user1",
                            "surname": "Black",
                            "user_id": 1
                        },
                        {
                            "created_at": "2018-11-07T06:46:43.000Z",
                            "name": "Billy",
                            "post": "7 post user2",
                            "surname": "White",
                            "user_id": 2
                        },
                        {
                            "created_at": "2018-11-07T06:46:43.000Z",
                            "name": "Gek",
                            "post": "7 post user1",
                            "surname": "Black",
                            "user_id": 1
                        },
                        {
                            "created_at": "2018-11-08T06:46:43.000Z",
                            "name": "Billy",
                            "post": "8 post user2",
                            "surname": "White",
                            "user_id": 2
                        }
            ]});
        });
    });
});