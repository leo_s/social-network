const chai = require('chai');
const chaiHttp = require('chai-http');
const models = require('../../models');


const {expect} = chai;
chai.use(chaiHttp);

/*global describe before it after*/
describe('Auth Router', function () {
    after(async function () {
        await models.sequelize.sync({ force: true });
    });

    describe('Create first account', function () {
        before(async function () {
            this.response = await chai.request(this.app)
                .post(`/api/createProfile`)
                .send({
                    password: '1qaz!QAZ',
                    name: 'Gek',
                    surname: 'Black',
                    email: 'gek-black@gmail.com',
                });
        });

        it('should respond with 200', function () {
            expect(this.response).to.have.status(200);
        });
    });

    describe('Create second account', function () {
        before(async function () {
            this.response = await chai.request(this.app)
                .post(`/api/createProfile`)
                .send({
                    password: '!QAZ1qaz',
                    name: 'Billy',
                    surname: 'White',
                    email: 'billy-white@gmail.com',
                });
        });

        it('should respond with 200', function () {
            expect(this.response).to.have.status(200);
        });
    });

    describe('POST /api/login', function () {
        before(async function () {
            this.response = await chai.request(this.app)
                .post('/api/login').send({
                    email: 'billy-white@gmail.com',
                    password: '!QAZ1qaz',
                });
        });

        it('should respond with 200', function () {
            expect(this.response).to.have.status(200);
        });

        it('should respond with correct body', function () {
            expect(this.response.body).to.deep.equals({
                email: 'billy-white@gmail.com',
                id: 2,
            });
        });
    });

    describe('GET /api/logout', function () {
        before(async function () {
            const agent = chai.request.agent(this.app);
            await agent.post('/api/login').send({
                email: 'billy-white@gmail.com',
                password: '!QAZ1qaz',
            });
            this.response = await agent
                .get(`/api/logout`)
                .send();
            this.response = await agent
                .get(`/api/loadMyProfile`)
                .send();
        });

        it('should respond with 401', function () {
            expect(this.response).to.have.status(401);
        });

        it('should respond with correct message', function () {
            expect(this.response.text).to.equals('Unauthenticated');
        });
    });

    describe('GET /api/loadMyProfile', function () {
        describe('when not authenticated', function () {
            before(async function () {
                this.response = await chai.request(this.app)
                    .get(`/api/loadMyProfile`)
                    .send();
            });

            it('should respond with 401', function () {
                expect(this.response).to.have.status(401);
            });

            it('should respond with correct message', function () {
                expect(this.response.text).to.equals('Unauthenticated');
            });
        });


        describe('when authenticated', function () {
            before(async function () {
                const agent = chai.request.agent(this.app);
                await agent.post('/api/login').send({
                    email: 'billy-white@gmail.com',
                    password: '!QAZ1qaz',
                });
                this.response = await agent
                    .get(`/api/loadMyProfile`)
                    .send();
            });

            it('should respond with 200', function () {
                expect(this.response).to.have.status(200);
            });

            it('should respond with correct body', function () {
                expect(this.response.body).to.deep.equals({
                    email: 'billy-white@gmail.com',
                    name: 'Billy',
                    surname: "White",
                    id: 2,
                });
            });
        });

    });

    describe('login with missing email', function () {
        before(async function () {
            this.response = await chai.request(this.app)
                .post('/api/login').send({
                    email: 'missing@gmail.com',
                    password: '!QAZ1qaz',
                });
        });

        it('should respond with 401', function () {
            expect(this.response).to.have.status(401);
        });

        it('should respond with correct message', function () {
            expect(this.response.text).to.equals('Incorrect login/password');
        });
    });

});
