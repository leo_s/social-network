const chai = require('chai');
const chaiHttp = require('chai-http');
const models = require('../../models');

const {expect} = chai;
chai.use(chaiHttp);

/*global describe before it after*/
describe('Test friend relation', function () {
    after(async function () {
        await models.sequelize.sync({ force: true });
    });
    before(async function() {
        this.timeout(10000);
    });

    const accounts = [
        {'password': '1qaz!QAZ', 'name': 'Gek', 'surname': 'Black', 'email': '111@gmail.com',},
        {'password': '1qaz!QAZ', 'name': 'Billy', 'surname': 'White', 'email': '222@gmail.com',},
        {'password': '1qaz!QAZ', 'name': 'Gek', 'surname': 'Grey', 'email': '333@gmail.com',},
        {'password': '1qaz!QAZ', 'name': 'Billy', 'surname': 'Grey', 'email': '444@gmail.com',},
        {'password': '1qaz!QAZ', 'name': 'Петя', 'surname': 'Пончик', 'email': '555@gmail.com',}
    ];

    accounts.forEach(function (account) {
        describe('Create accounts', function () {
            before(async function () {
                this.response = await chai.request(this.app)
                    .post(`/api/createProfile`)
                    .send({
                        password: account.password,
                        name: account.name,
                        surname: account.surname,
                        email: account.email,
                    });
            });

            it('should respond with 200', function () {
                expect(this.response).to.have.status(200);
            });
        });
    });

    describe('POST. User 1 sends one friend query for the persons 2, 3, 4', function () {
        before(async function () {
            const agent = chai.request.agent(this.app);
            await agent.post('/api/login').send({
                email: '111@gmail.com',
                password: '1qaz!QAZ',
            });
            const friends = JSON.stringify(['2', '3', '4']);
            // const friends = "friends":"2";
            this.response = await agent
                .post('/api/friendQuery').send({
                    friends: friends,
                });
        });

        it('should respond with 200', function () {
            expect(this.response).to.have.status(200);
        });

    });

    describe('POST. User 2 sends one friend query for the persons 1, 3, 4', function () {
        before(async function () {
            const agent = chai.request.agent(this.app);
            await agent.post('/api/login').send({
                email: '222@gmail.com',
                password: '1qaz!QAZ',
            });
            const friends = JSON.stringify(['1', '3', '4']);
            this.response = await agent
                .post('/api/friendQuery').send({
                    friends: friends,
                });
        });

        it('should respond with 200', function () {
            expect(this.response).to.have.status(200);
        });
    });

    describe('POST. User 3 sends one friend confirm for the persons 1, 2', function () {
        before(async function () {
            const friends = JSON.stringify(['1', '2']);
            const agent = chai.request.agent(this.app);
            await agent.post('/api/login').send({
                email: '333@gmail.com',
                password: '1qaz!QAZ',
            });
            this.response = await agent
                .put('/api/friendConfirm').send({
                    friends: friends,
                });
        });

        it('should respond with 200', function () {
            expect(this.response).to.have.status(200);
        });
    });

    describe('POST. User 5 sends one friend query for the persons 1, 2, 4', function () {
        before(async function () {
            const agent = chai.request.agent(this.app);
            await agent.post('/api/login').send({
                email: '555@gmail.com',
                password: '1qaz!QAZ',
            });
            const friends = JSON.stringify(['1', '2', '4']);
            this.response = await agent
                .post('/api/friendQuery').send({
                    friends: friends,
                });
        });

        it('should respond with 200', function () {
            expect(this.response).to.have.status(200);
        });
    });

    describe('GET. User 2 demands his friends. There are ' +
        'two confirmed friends: ' +
        '(1->2) 1 had queried, 2 has confirmed by the counter query, ' +
        '(2->3) 2 had queried, 3 has confirmed. ', function () {
        before(async function () {
            //const friends = JSON.stringify({'1':'2','2':'3','3':'4'});
            //const friends = JSON.stringify(['1','2']);
            const agent = chai.request.agent(this.app);
            await agent.post('/api/login').send({
                email: '222@gmail.com',
                password: '1qaz!QAZ',
            });
            this.response = await agent
                .get('/api/friendsList/1')
                .send();
        });

        it('should respond with 200', function () {
            expect(this.response).to.have.status(200);
        });

        it('should respond with correct body', function () {
            const input = JSON.parse(this.response.text);
            expect(input).to.deep.equals(
                {
                    "count": 2,
                    "rows": [
                        {
                            "confirmed": 1,
                            "id": 3,
                            "master_id": 2,
                            "name": "Gek",
                            "surname": "Grey"
                        },
                        {
                            "confirmed": 1,
                            "id": 1,
                            "master_id": 2,
                            "name": "Gek",
                            "surname": "Black"
                        }
                    ]
                }
            );
        });
    });

    describe('GET. User 2 demands his queries. There are ' +
        'one non confirmed friendship: ' +
        '(2->4) 2 had queried, but 4 has not confirmed yet', function () {
        before(async function () {
            const agent = chai.request.agent(this.app);
            await agent.post('/api/login').send({
                email: '222@gmail.com',
                password: '1qaz!QAZ',
            });
            this.response = await agent
                .get('/api/friendsList/0')
                .send();
        });

        it('should respond with 200', function () {
            expect(this.response).to.have.status(200);
        });

        it('should respond with correct body', function () {
            const input = JSON.parse(this.response.text);
            expect(input).to.deep.equals(
                {
                    "count": 1,
                    "rows": [
                        {
                            "confirmed": 0,
                            "id": 4,
                            "master_id": 2,
                            "name": "Billy",
                            "surname": "Grey"
                        }
                    ]
                }
            );
        });
    });

    describe('GET. Who has queried User 2 friendship. There are ' +
        'one non confirmed friendship: ' +
        '(5->2) 5 had queried, but 2 has not confirmed yet', function () {
        before(async function () {
            //const friends = JSON.stringify({'1':'2','2':'3','3':'4'});
            //const friends = JSON.stringify(['1','2']);
            const agent = chai.request.agent(this.app);
            await agent.post('/api/login').send({
                email: '222@gmail.com',
                password: '1qaz!QAZ',
            });
            this.response = await agent
                .get('/api/queryToMeList')
                .send();
        });

        it('should respond with 200', function () {
            expect(this.response).to.have.status(200);
        });

        it('should respond with correct body', function () {
            const input = JSON.parse(this.response.text);
            expect(input).to.deep.equals(
                {
                    "count": 1,
                    "rows": [
                        {
                            "confirmed": false,
                            "id": 5,
                            "name": "Петя",
                            "surname": "Пончик",
                            "user_id": 2
                        }
                    ]
                }
            );
        });
    });

    describe('POST. User 1 deletes the persons 2 out of his friends', function () {
        before(async function () {
            const agent = chai.request.agent(this.app);
            await agent.post('/api/login').send({
                email: '111@gmail.com',
                password: '1qaz!QAZ',
            });
            // const friend = JSON.stringify(['2']);
            this.response = await agent
                .put('/api/friendDelete').send({
                    friend: 2,
                });
        });

        it('should respond with 200', function () {
            expect(this.response).to.have.status(200);
        });

        it('should respond with correct body', function () {
            expect(this.response.text).to.deep.equals('Friend id 2 is deleted!');
        });

    });

    describe('POST. User 1 deletes out the friend request to persons 4', function () {
        before(async function () {
            const agent = chai.request.agent(this.app);
            await agent.post('/api/login').send({
                email: '111@gmail.com',
                password: '1qaz!QAZ',
            });
            // const friend = JSON.stringify(['2']);
            this.response = await agent
                .put('/api/queryDelete').send({
                    friend: 4,
                });
        });

        it('should respond with 200', function () {
            expect(this.response).to.have.status(200);
        });

        it('should respond with correct body', function () {
            expect(this.response.text).to.deep.equals('Query to the person id 4 is deleted!');
        });
    });
});