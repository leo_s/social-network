const chai = require('chai');
const chaiHttp = require('chai-http');
const models = require('../../models');

const {expect} = chai;
chai.use(chaiHttp);

/*global describe before it after*/
describe('GET /api/usersList', function () {
    after(async function () {
        await models.sequelize.sync({ force: true });
    });

    const accounts = [
        {'password': '1qaz!QAZ', 'name': 'Gek', 'surname': 'Black', 'email': '111@gmail.com',},
        {'password': '1qaz!QAZ', 'name': 'Billy', 'surname': 'White', 'email': '222@gmail.com',},
        {'password': '1qaz!QAZ', 'name': 'Gek', 'surname': 'Grey', 'email': '333@gmail.com',},
        {'password': '1qaz!QAZ', 'name': 'Billy', 'surname': 'Grey', 'email': '444@gmail.com',},
        {'password': '1qaz!QAZ', 'name': 'Петя', 'surname': 'Пончик', 'email': '555@gmail.com',}
    ];

    accounts.forEach(function (account) {
        describe('Create accounts', function () {
            before(async function () {
                this.response = await chai.request(this.app)
                    .post(`/api/createProfile`)
                    .send({
                        password: account.password,
                        name: account.name,
                        surname: account.surname,
                        email: account.email,
                    });
                account.id = this.response.body;
            });

            it('should respond with 200', function () {
                expect(this.response).to.have.status(200);
            });
        });
    });

    describe('Get users list when not authenticated', function () {
        before(async function () {
            this.response = await chai.request(this.app)
                .get(`/api/usersList`)
                .send();
        });

        it('should respond with 401', function () {
            expect(this.response).to.have.status(401);
        });

        it('should respond with correct message', function () {
            expect(this.response.text).to.equals('Unauthenticated');
        });
    });

    describe('Get users list when authenticated', function () {
        before(async function () {
            const agent = chai.request.agent(this.app);
            await agent.post('/api/login').send({
                email: '222@gmail.com',
                password: '1qaz!QAZ',
            });
            this.response = await agent
                .get(`/api/usersList`)
                .send();
        });

        it('should respond with 200', function () {
            expect(this.response).to.have.status(200);
        });

        it('should respond with correct body', function () {
            expect(this.response.body).to.deep.equals([
                {
                    id: 1,
                    name: "Gek",
                    surname: "Black",
                },
                {
                    id: 2,
                    name: "Billy",
                    surname: "White",
                },
                {
                    id: 3,
                    name: "Gek",
                    surname: "Grey",
                },
                {
                    id: 4,
                    name: "Billy",
                    surname: "Grey",
                },
                {
                    id: 5,
                    name: "Петя",
                    surname: "Пончик",
                },
            ]);
        });
    });

    describe('GET list by the first symbols of the name only', function () {
        before(async function () {
            const agent = chai.request.agent(this.app);
            await agent.post('/api/login').send({
                email: '222@gmail.com',
                password: '1qaz!QAZ',
            });
            this.response = await agent
                .get(`/api/usersList/filter/bil`)
                .send();

        });

        it('should respond with 200', function () {
            expect(this.response).to.have.status(200);
        });

        it('should respond with correct body', function () {
            expect(this.response.body).to.deep.equals([
                    {
                        "confirmed": null,
                        "id": 2,
                        "masterId": null,
                        "name": "Billy",
                        "surname": "White"
                    },
                    {
                        "confirmed": null,
                        "id": 4,
                        "masterId": null,
                        "name": "Billy",
                        "surname": "Grey"
                    }
            ]);
        });
    });

    describe('GET list by the first symbols of the surname only', function () {
        before(async function () {
            const agent = chai.request.agent(this.app);
            await agent.post('/api/login').send({
                email: '222@gmail.com',
                password: '1qaz!QAZ',
            });
            this.response = await agent
                .get(`/api/usersList/filter/gr`)
                .send();

        });

        it('should respond with 200', function () {
            expect(this.response).to.have.status(200);
        });

        it('should respond with correct body', function () {
            expect(this.response.body).to.deep.equals([
                {
                    "confirmed": null,
                    "id": 3,
                    "masterId": null,
                    "name": "Gek",
                    "surname": "Grey"
                },
                {
                    "confirmed": null,
                    "id": 4,
                    "masterId": null,
                    "name": "Billy",
                    "surname": "Grey"
                }
            ]);
        });
    });

    describe('GET list by the request where the first symbols of the name before the surname', function () {
        before(async function () {
            const url = encodeURI('/api/usersList/filter/Ge Gr');
            const agent = chai.request.agent(this.app);
            await agent.post('/api/login').send({
                email: '222@gmail.com',
                password: '1qaz!QAZ',
            });
            this.response = await agent
                .get(url)
                .send();

        });

        it('should respond with 200', function () {
            expect(this.response).to.have.status(200);
        });

        it('should respond with correct body', function () {
            expect(this.response.body).to.deep.equals([
                {
                    "confirmed": null,
                    "id": 3,
                    "masterId": null,
                    "name": "Gek",
                    "surname": "Grey"
                }
            ]);
        });
    });

    describe('GET list by the request where the first symbols of the surname before the name', function () {
        before(async function () {
            const url = encodeURI('/api/usersList/filter/Gr Ge');
            const agent = chai.request.agent(this.app);
            await agent.post('/api/login').send({
                email: '222@gmail.com',
                password: '1qaz!QAZ',
            });
            this.response = await agent
                .get(url)
                .send();

        });

        it('should respond with 200', function () {
            expect(this.response).to.have.status(200);
        });

        it('should respond with correct body', function () {
            expect(this.response.body).to.deep.equals([
                {
                    "confirmed": null,
                    "id": 3,
                    "masterId": null,
                    "name": "Gek",
                    "surname": "Grey"
                }
            ]);
        });
    });

    describe('GET list by the request where the name and the surname has a Cyrillic symbols', function () {
        before(async function () {
            const url = encodeURI('/api/usersList/filter/Пончик Петя');
            const agent = chai.request.agent(this.app);
            await agent.post('/api/login').send({
                email: '222@gmail.com',
                password: '1qaz!QAZ',
            });
            this.response = await agent
                .get(url)
                .send();

        });

        it('should respond with 200', function () {
            expect(this.response).to.have.status(200);
        });

        it('should respond with correct body', function () {
            expect(this.response.body).to.deep.equals([
                {
                    "confirmed": null,
                    "id": 5,
                    "masterId": null,
                    "name": "Петя",
                    "surname": "Пончик"
                }
            ]);
        });
    });
});

