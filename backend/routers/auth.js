const passport = require('../middlewares/passport');

const login = async (ctx) => {
  if (ctx.isAuthenticated()) {
    return ctx.redirect('/');
  }

    await passport.authenticate('local', {}, async (err, user) => {
    if (!user) {
      ctx.throw(401, 'Incorrect login/password');
    }

    ctx.login(user, (err) => {
      if (err) {
        ctx.throw(401, err.message);
      }
      ctx.status = 200;
      ctx.body = user;
    });
  })(ctx);
};

const logout = async (ctx) => {
  await ctx.logout();
  return ctx.redirect('/');
};

module.exports = {
  login,
  logout,
};