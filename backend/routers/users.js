const usersList = async (ctx) => {
    try {
        if (ctx.isUnauthenticated()) {
            ctx.throw(401, 'Unauthenticated');
        }
        ctx.status = 200;
        const out = await ctx.models.User.findAll({
            attributes: ['id', 'name', 'surname']
        });
        ctx.body = out;
    } catch (err) {
        ctx.throw(500, err)
    }

};

const usersListFilter = async (ctx) => {
    try {
        if (ctx.isUnauthenticated()) {
            ctx.throw(401, 'Unauthenticated');
        }
        ctx.status = 200;
        const person = ctx.params.person.split(/\s+/);
            if (person.length == 1) {//name or surname only.
                ctx.body = await ctx.models.User.findAll({
                    where: {//one syllable
                        [ctx.models.Sequelize.Op.or]: [
                            {name: {[ctx.models.Sequelize.Op.like]: person[0] + '%'}},
                            {surname: {[ctx.models.Sequelize.Op.like]: person[0] + '%'}}
                        ]
                    },
                    attributes: ['id', 'name', 'surname',
                        [ctx.models.Sequelize.col('friends.confirmed'), 'confirmed'],
                        [ctx.models.Sequelize.col('friends.master_id'), 'masterId'],
                    ],
                    includeIgnoreAttributes: false,
                    include: [{
                        model: ctx.models.Friend,
                        as: 'friends',
                        where: {
                            master_id: ctx.req.user.id,
                        },
                        required:false
                    }]
                });
            } else {//two syllables
            ctx.body =  await ctx.models.User.findAll({
                where: {
                    [ctx.models.Sequelize.Op.or]: [
                        {
                            name: {[ctx.models.Sequelize.Op.like]: person[0] + '%'},
                            surname: {[ctx.models.Sequelize.Op.like]: person[1] + '%'}
                        },
                        {
                            name: {[ctx.models.Sequelize.Op.like]: person[1] + '%'},
                            surname: {[ctx.models.Sequelize.Op.like]: person[0] + '%'}
                        },
                        ]
                },
                attributes: ['id', 'name', 'surname',
                    [ctx.models.Sequelize.col('friends.confirmed'), 'confirmed'],
                    [ctx.models.Sequelize.col('friends.master_id'), 'masterId'],
                ],
                includeIgnoreAttributes: false,
                include: [{
                    model: ctx.models.Friend,
                    as: 'friends',
                    where: {
                        master_id: ctx.req.user.id,
                    },
                    required:false
                }]
            });
        }
    } catch (err) {
        ctx.throw(500, err)
    }

};

const friendQuery = async (ctx) => {
    try {
        if (ctx.isUnauthenticated()) {
            ctx.throw(401, 'Unauthenticated');
        }
        ctx.status = 200;
        let {friends} = ctx.request.body;
        const friendsObj = JSON.parse(friends);
        friendsObj.forEach(async function (friendId) {
                const checkExistedQuery = await ctx.models.Friend.find({
                    where: {
                        master_id: friendId,
                        user_id: ctx.req.user.id,
                    },
                    attributes: ['user_id', 'master_id', 'confirmed']
                });
                if (checkExistedQuery) {
                    await ctx.models.Friend.update({
                        confirmed: true,
                    }, {
                        where: {
                            master_id: friendId,
                            user_id: ctx.req.user.id,
                        }
                    });
                    await ctx.models.Friend.create({ //create counter relation
                        master_id: ctx.req.user.id,
                        user_id: friendId,
                        confirmed: true,
                    });
                } else {
                    await ctx.models.Friend.create({
                        master_id: ctx.req.user.id,
                        user_id: friendId,
                    });
                }
            }
        );
        ctx.body = 'The query is sent. Await confirmation, please';
    } catch (err) {
        // ctx.throw(500, err)
        ctx.throw(500, ctx.request.body.friends)
    }

};
const friendConfirm = async (ctx) => {
    try {
        if (ctx.isUnauthenticated()) {
            ctx.throw(401, 'Unauthenticated');
        }
        ctx.status = 200;
        let {friends} = ctx.request.body;
        const friendsObj = JSON.parse(friends);
        friendsObj.forEach(async function (friendId) {
                const checkExistedQuery = await ctx.models.Friend.find({
                    where: {
                        master_id: friendId,
                        user_id: ctx.req.user.id,
                    },
                    attributes: ['user_id', 'master_id', 'confirmed']
                });
                if (checkExistedQuery) {
                    await ctx.models.Friend.update({
                        confirmed: true,
                    }, {
                        where: {
                            master_id: friendId,
                            user_id: ctx.req.user.id,
                        }
                    });
                    await ctx.models.Friend.create({ //create counter relation
                        master_id: ctx.req.user.id,
                        user_id: friendId,
                        confirmed: true,
                    });
                }
            }
        );
        ctx.body = 'The friendship is confirmed.';
    } catch (err) {
        ctx.throw(500, err)
    }
};

const friendsList = async (ctx) => {
    try {
        if (ctx.isUnauthenticated()) {
            ctx.throw(401, 'Unauthenticated');
        }
        ctx.status = 200;
        const confirmed = ctx.params.confirmed;
        ctx.body = await ctx.models.User.findAndCountAll({
            attributes: ['id', 'name', 'surname',
                [ctx.models.Sequelize.col('friends.master_id'), 'master_id'],
                [ctx.models.Sequelize.col('friends.confirmed'), 'confirmed'],
            ],
            includeIgnoreAttributes: false,
            include: {
                model: ctx.models.Friend,
                as: 'friends',
                where: {
                    master_id: ctx.req.user.id,
                    confirmed: confirmed,
                },
            },
        })
    } catch (err) {
        ctx.throw(500, err)
    }
};

const queryToMeList = async (ctx) => {
    try {
        if (ctx.isUnauthenticated()) {
            ctx.throw(401, 'Unauthenticated');
        }
        ctx.status = 200;
        ctx.body = await ctx.models.Friend.findAndCountAll({
            attributes: ['user_id', 'confirmed',
                [ctx.models.Sequelize.col('Friend.master_id'), 'id'],
                [ctx.models.Sequelize.col('master.name'), 'name'],
                [ctx.models.Sequelize.col('master.surname'), 'surname'],
            ],
            includeIgnoreAttributes: false,
            where: {
                user_id: ctx.req.user.id,
                confirmed: false,
            },
            include: {
                model: ctx.models.User,
                as: 'master',
                attributes: ['name', 'surname',]
            },
        });
    } catch (err) {
        ctx.throw(500, err)
    }
};

const friendDelete = async (ctx) => {
    try {
        let {friend} = ctx.request.body;
        await ctx.models.Friend.destroy({
                where: {
                    [ctx.models.Sequelize.Op.or]: [
                        {user_id: friend, master_id: ctx.req.user.id},
                        {user_id: ctx.req.user.id, master_id: friend}
                    ]
                }
            }
        )
        ctx.body = `Friend id ${friend} is deleted!`;
    }
    catch (err) {
        ctx.throw(500, err);
    }
}

const queryDelete = async (ctx) => {
    try {
        let {friend} = ctx.request.body;
        await ctx.models.Friend.destroy({
                where: {
                    user_id: friend, master_id: ctx.req.user.id
                }
            }
        )
        ctx.body = `Query to the person id ${friend} is deleted!`;
    }
    catch (err) {
        ctx.throw(500, err);
    }
}

module.exports = {
    usersList,
    usersListFilter,
    friendQuery,
    friendConfirm,
    friendsList,
    queryToMeList,
    friendDelete,
    queryDelete
};