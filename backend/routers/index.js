const Router = require('koa-router');
const {
    login,
    logout,
} = require('./auth');

const {
    editProfile,
    loadMyProfile,
    loadUserProfile,
    createProfile
} = require('./profile');

const {
    usersList,
    usersListFilter,
    friendQuery,
    friendConfirm,
    friendDelete,
    friendsList,
    queryToMeList,
    queryDelete,
} = require('./users');

const {
    createPost,
    showTape
} = require('./post');

const router = new Router();

router.post('/api/createProfile', createProfile);
router.get('/api/loadMyProfile', loadMyProfile);
router.get('/api/loadUserProfile/:userId', loadUserProfile);
router.put('/api/editProfile', editProfile);
router.post('/api/login', login);
router.get('/api/logout', logout);
router.get('/api/usersList', usersList);
router.get('/api/usersList/filter/:person', usersListFilter);
router.post('/api/friendQuery', friendQuery);
router.put('/api/friendConfirm', friendConfirm);
router.put('/api/friendDelete', friendDelete);
router.put('/api/queryDelete', queryDelete);
router.get('/api/friendsList/:confirmed', friendsList);
router.get('/api/queryToMeList', queryToMeList);
router.post('/api/createPost', createPost);
router.get('/api/showTape/:page', showTape);

module.exports = router;