const UtilService = require('../../services/util.service');

const createProfile = async (ctx) => {
    try {
        let {name, surname, email, password} = ctx.request.body;
        if(/(?=^.{6,10}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&amp;*()_+}{&quot;:;'?/&gt;.&lt;,])(?!.*\s).*$/.test(password) === false){
            ctx.throw(400, 'Password password should be at 6-10 characters, contain at least one number, one lowercase letter, one uppercase letter, one special character')
        }
        const encryptedPassword = await UtilService.hashPassword(password);
        await ctx.models.User.create({
            name,
            surname,
            email,
            password: encryptedPassword
        }).catch(
            error => {
                if (error.name.localeCompare('SequelizeValidationError') === 0){
                    ctx.throw(400, error.message)
                } else if ( error.name.localeCompare('SequelizeUniqueConstraintError') === 0){
                    ctx.throw(400, `${email} had already registered, try another please!`)
                } else {
                    throw error;
                }
            }
        );
        ctx.body = 'Account is created!';
    }
    catch (err) {
        ctx.throw(500, err);
    }
}

const loadMyProfile = (ctx) => {
    if (ctx.isUnauthenticated()) {
        ctx.throw(401, 'Unauthenticated');
    }
    ctx.status = 200;
    ctx.body = ctx.state.user;
};

const loadUserProfile = async (ctx) => {
    try {
        if (ctx.isUnauthenticated()) {
            ctx.status = 200;
            const userId = ctx.params.userId;
            ctx.body = await ctx.models.User.find({
                attributes: ['name', 'surname'],
                includeIgnoreAttributes: false,
                where: {
                    id: userId,
                },
            });
        }
        else {
            ctx.status = 200;
            const userId = ctx.params.userId;
            ctx.body = await ctx.models.User.find({
                where: {
                    id: userId,
                },
                attributes: ['name', 'surname', 'email'],
                includeIgnoreAttributes: false,
            });
        }
    } catch (err) {
        ctx.throw(500, err)
    }
};

const editProfile = async (ctx) => {
    try {
        let {name, surname, email} = ctx.request.body;
        await ctx.models.User.update({
            name: name,
            surname: surname,
            email: email,
        }, {
            where: {id: ctx.req.user.id}
        }
        ).catch(
            error => {
                if (error.name.localeCompare('SequelizeValidationError') === 0 ||
                    error.name.localeCompare('SequelizeUniqueConstraintError') === 0){
                    ctx.throw(400, error.message)
                } else {
                    throw error;
                }
            }
        );
        ctx.body = 'Account is modified!';
    }
    catch (err) {
        ctx.throw(500, err);
    }
}


module.exports = {
    loadMyProfile,
    loadUserProfile,
    createProfile,
    editProfile
};