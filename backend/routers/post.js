const createPost = async (ctx) => {
    try {
        let {post} = ctx.request.body;
        await ctx.models.Post.create({
            post,
            user_id: ctx.req.user.id,
        }).catch(
            error => {
                if (error.name.localeCompare('SequelizeValidationError') === 0){
                    ctx.throw(400, error.message)
                } else {
                    throw error;
                }
            }
        );
        ctx.body = 'Your post is written.';
    }
    catch (err) {
        ctx.throw(500, err);
    }

}

const showTape = async (ctx) => {
    try {
        if (ctx.isUnauthenticated()) {
            ctx.throw(401, 'Unauthenticated');
        }
        ctx.status = 200;
        const page = ctx.params.page;
        const out = await ctx.models.Post.findAndCountAll({
            attributes : [
                'post',
                'user_id',
                'created_at',
                [ctx.models.Sequelize.col('User.name'), 'name'],
                [ctx.models.Sequelize.col('User.surname'), 'surname']
            ],
            includeIgnoreAttributes: false,
            order: ['created_at', ctx.models.Sequelize.col('User.name')],
            subQuery: false, //without subQuery: false, with limit: 5 it returns error
            offset: page * 5,
            limit: 5,
            include: [{
                model: ctx.models.User,
                required: true,
                include: [{
                    model: ctx.models.Friend,
                    as: 'friends',
                    required: true,
                    where: {
                        confirmed: true,
                        master_id: ctx.req.user.id,
                    },
                }]
            }],
        });

        ctx.body = out;
    }
    catch (err) {
        ctx.throw(500, err)
    }
};

module.exports = {
    createPost,
    showTape,
};