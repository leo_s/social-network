const Sequelize = require('sequelize');
const env = process.env.NODE_ENV || 'development';
const {db} = require('../config')[env];

const sequelize = new Sequelize(db.name, db.username, db.password, {
    host: db.host,
    port: db.port,
    dialect: db.dialect,
    logging: db.logging,
    operatorsAliases: false
});


const User = require('./user')(sequelize);
const Friend = require('./friend')(sequelize);
const Post = require('./post')(sequelize);

const models = {
    [User.name]: User,
    [Friend.name]: Friend,
    [Post.name]: Post,
};

Object.keys(models).forEach((modelName) => {
    if (models[modelName].associate) {
        models[modelName].associate(models);
    }
});

models.sequelize = sequelize;
models.Sequelize = Sequelize;

module.exports = models;
