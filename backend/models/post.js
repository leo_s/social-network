const Sequelize = require('sequelize');

module.exports = (sequelize) => {
    const Post = sequelize.define('Post', {
        post: {
            type: Sequelize.STRING(2048),
            allowNull: false,
            validate: {
                notEmpty: true
            }
        },
    }, {
        underscored: true,
        tableName: 'posts',
        charset: 'utf8',
        collate: 'utf8_unicode_ci',
    });

    Post.associate = function (models) {
        Post.belongsTo(models.User, {
        });
    };

    return Post;
};