const Sequelize = require('sequelize');

module.exports = (sequelize) => {
    const Friend = sequelize.define('Friend', {
        confirmed:{
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }, {
        underscored: true,
        tableName: 'friends'
    });

    Friend.associate = function (models) {
        Friend.belongsTo(models.User, {
            as: 'master',
        });
    };

    return Friend;
};
