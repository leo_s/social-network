CREATE DATABASE social_network;
CREATE DATABASE sn_test;

CREATE USER 'sn_admin'@'localhost' IDENTIFIED BY '1qaz@WSX';
GRANT ALL PRIVILEGES ON social_network.* TO 'sn_admin'@'localhost';
GRANT ALL PRIVILEGES ON sn_test.* TO 'sn_admin'@'localhost';

!!!после запуска сервера выполнить -
mysql -u root -p social_network < ~/..../social-network/12Users100Posts.sql

в базе 12 пользователей сс именами
Aaa, Bbb, Ccc ... Jjj и одинаковой фамилией - Zzz
два пользователя с именем Zzz и разными фамилиями Aaa, Bbb
У всех пользователей одинаковый пароль - 1qaz@W


поиск по первым символам имени или фамилии (в любом порядке, строчные или прописные)

Просмотреть профиль пользователя неавторизованный пользователь может по ссылке
http://localhost:3000/showProfile/х (х от 1 - Aaa Zzz до 12 - Zzz Bbb)
из базы получаются только имя/фамилия - отображаются и e-mail - скрыт
остальные даннае от faker.js для "красоты"

Frontend - добавление поста ограничено 20 символами, для проверки.
Backend - ограничение 2048


# TT Developers School project template

This repo contains the template of the project for TT Developers School 2018.

## Requirements:
1. Node v10.x
2. NPM v5.x

## How to install requirements:
1. npm install
2. cd ./frontend && npm install

## How to run app:
1. npm start

## Repo contains:
1. docker-compose file with postgres db. You can change db_user and db_password in docker-compose.yml file.

## How to use Docker:
* npm run docker-build: to download images if it's still not exist
* npm run docker-start: start postgres within docker containers
* npm run docker-clean: turn containers off.

