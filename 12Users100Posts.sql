-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: social_network
-- ------------------------------------------------------
-- Server version	5.7.24-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `friends`
--

DROP TABLE IF EXISTS `friends`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `friends` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `master_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `master_id` (`master_id`),
  CONSTRAINT `friends_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `friends_ibfk_2` FOREIGN KEY (`master_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `friends`
--

LOCK TABLES `friends` WRITE;
/*!40000 ALTER TABLE `friends` DISABLE KEYS */;
INSERT INTO `friends` VALUES (1,1,'2018-12-29 13:58:09','2018-12-29 13:58:09',2,1),(2,1,'2018-12-29 13:58:09','2018-12-29 13:58:09',3,1),(3,1,'2018-12-29 13:58:09','2018-12-29 13:58:09',4,1),(4,1,'2018-12-29 13:58:09','2018-12-29 13:58:09',5,1),(5,1,'2018-12-29 13:58:09','2018-12-29 13:58:09',1,2),(6,0,'2018-12-29 13:58:09','2018-12-29 13:58:09',3,2),(7,0,'2018-12-29 13:58:09','2018-12-29 13:58:09',4,2),(8,0,'2018-12-29 13:58:09','2018-12-29 13:58:09',5,2),(9,1,'2018-12-29 13:58:09','2018-12-29 13:58:09',1,3),(10,0,'2018-12-29 13:58:09','2018-12-29 13:58:09',4,3),(11,0,'2018-12-29 13:58:09','2018-12-29 13:58:09',5,3),(12,1,'2018-12-29 13:58:09','2018-12-29 13:58:09',1,4),(13,0,'2018-12-29 13:58:09','2018-12-29 13:58:09',5,4),(14,1,'2018-12-29 13:58:09','2018-12-29 13:58:09',1,5);
/*!40000 ALTER TABLE `friends` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post` varchar(2048) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,'abcdefghijklmnopqrstuvwxyz 111@gmail.com','2018-01-01 13:00:00','2018-01-01 13:00:00',1),(2,'bcdefghijklmnopqrstuvwxyz 111@gmail.com','2018-02-01 13:00:00','2018-02-01 13:00:00',1),(3,'cdefghijklmnopqrstuvwxyz 111@gmail.com','2018-03-01 13:00:00','2018-03-01 13:00:00',1),(4,'defghijklmnopqrstuvwxyz 111@gmail.com','2018-04-01 13:00:00','2018-04-01 13:00:00',1),(5,'efghijklmnopqrstuvwxyz 111@gmail.com','2018-05-01 13:00:00','2018-05-01 13:00:00',1),(6,'fghijklmnopqrstuvwxyz 111@gmail.com','2018-06-01 13:00:00','2018-06-01 13:00:00',1),(7,'ghijklmnopqrstuvwxyz 111@gmail.com','2018-06-01 14:00:00','2018-06-01 14:00:00',1),(8,'hijklmnopqrstuvwxyz 111@gmail.com','2018-06-01 15:00:00','2018-06-01 15:00:00',1),(9,'ijklmnopqrstuvwxyz 111@gmail.com','2018-06-01 16:00:00','2018-06-01 16:00:00',1),(10,'jklmnopqrstuvwxyz 111@gmail.com','2018-06-01 17:00:00','2018-06-01 17:00:00',1),(11,'abcdefghijklmnopqrstuvwxyz 222@gmail.com','2018-01-01 13:00:00','2018-01-01 13:00:00',2),(12,'bcdefghijklmnopqrstuvwxyz 222@gmail.com','2018-02-01 13:00:00','2018-02-01 13:00:00',2),(13,'cdefghijklmnopqrstuvwxyz 222@gmail.com','2018-03-01 13:00:00','2018-03-01 13:00:00',2),(14,'defghijklmnopqrstuvwxyz 222@gmail.com','2018-04-01 13:00:00','2018-04-01 13:00:00',2),(15,'efghijklmnopqrstuvwxyz 222@gmail.com','2018-05-01 13:00:00','2018-05-01 13:00:00',2),(16,'fghijklmnopqrstuvwxyz 222@gmail.com','2018-06-01 13:00:00','2018-06-01 13:00:00',2),(17,'ghijklmnopqrstuvwxyz 222@gmail.com','2018-06-01 14:00:00','2018-06-01 14:00:00',2),(18,'hijklmnopqrstuvwxyz 222@gmail.com','2018-06-01 15:00:00','2018-06-01 15:00:00',2),(19,'ijklmnopqrstuvwxyz 222@gmail.com','2018-06-01 16:00:00','2018-06-01 16:00:00',2),(20,'jklmnopqrstuvwxyz 222@gmail.com','2018-06-01 17:00:00','2018-06-01 17:00:00',2),(21,'abcdefghijklmnopqrstuvwxyz 333@gmail.com','2018-01-01 13:00:00','2018-01-01 13:00:00',3),(22,'bcdefghijklmnopqrstuvwxyz 333@gmail.com','2018-02-01 13:00:00','2018-02-01 13:00:00',3),(23,'cdefghijklmnopqrstuvwxyz 333@gmail.com','2018-03-01 13:00:00','2018-03-01 13:00:00',3),(24,'defghijklmnopqrstuvwxyz 333@gmail.com','2018-04-01 13:00:00','2018-04-01 13:00:00',3),(25,'efghijklmnopqrstuvwxyz 333@gmail.com','2018-05-01 13:00:00','2018-05-01 13:00:00',3),(26,'fghijklmnopqrstuvwxyz 333@gmail.com','2018-06-01 13:00:00','2018-06-01 13:00:00',3),(27,'ghijklmnopqrstuvwxyz 333@gmail.com','2018-06-01 14:00:00','2018-06-01 14:00:00',3),(28,'hijklmnopqrstuvwxyz 333@gmail.com','2018-06-01 15:00:00','2018-06-01 15:00:00',3),(29,'ijklmnopqrstuvwxyz 333@gmail.com','2018-06-01 16:00:00','2018-06-01 16:00:00',3),(30,'jklmnopqrstuvwxyz 333@gmail.com','2018-06-01 17:00:00','2018-06-01 17:00:00',3),(31,'abcdefghijklmnopqrstuvwxyz 444@gmail.com','2018-01-01 13:00:00','2018-01-01 13:00:00',4),(32,'bcdefghijklmnopqrstuvwxyz 444@gmail.com','2018-02-01 13:00:00','2018-02-01 13:00:00',4),(33,'cdefghijklmnopqrstuvwxyz 444@gmail.com','2018-03-01 13:00:00','2018-03-01 13:00:00',4),(34,'defghijklmnopqrstuvwxyz 444@gmail.com','2018-04-01 13:00:00','2018-04-01 13:00:00',4),(35,'efghijklmnopqrstuvwxyz 444@gmail.com','2018-05-01 13:00:00','2018-05-01 13:00:00',4),(36,'fghijklmnopqrstuvwxyz 444@gmail.com','2018-06-01 13:00:00','2018-06-01 13:00:00',4),(37,'ghijklmnopqrstuvwxyz 444@gmail.com','2018-06-01 14:00:00','2018-06-01 14:00:00',4),(38,'hijklmnopqrstuvwxyz 444@gmail.com','2018-06-01 15:00:00','2018-06-01 15:00:00',4),(39,'ijklmnopqrstuvwxyz 444@gmail.com','2018-06-01 16:00:00','2018-06-01 16:00:00',4),(40,'jklmnopqrstuvwxyz 444@gmail.com','2018-06-01 17:00:00','2018-06-01 17:00:00',4),(41,'abcdefghijklmnopqrstuvwxyz 555@gmail.com','2018-01-01 13:00:00','2018-01-01 13:00:00',5),(42,'bcdefghijklmnopqrstuvwxyz 555@gmail.com','2018-02-01 13:00:00','2018-02-01 13:00:00',5),(43,'cdefghijklmnopqrstuvwxyz 555@gmail.com','2018-03-01 13:00:00','2018-03-01 13:00:00',5),(44,'defghijklmnopqrstuvwxyz 555@gmail.com','2018-04-01 13:00:00','2018-04-01 13:00:00',5),(45,'efghijklmnopqrstuvwxyz 555@gmail.com','2018-05-01 13:00:00','2018-05-01 13:00:00',5),(46,'fghijklmnopqrstuvwxyz 555@gmail.com','2018-06-01 13:00:00','2018-06-01 13:00:00',5),(47,'ghijklmnopqrstuvwxyz 555@gmail.com','2018-06-01 14:00:00','2018-06-01 14:00:00',5),(48,'hijklmnopqrstuvwxyz 555@gmail.com','2018-06-01 15:00:00','2018-06-01 15:00:00',5),(49,'ijklmnopqrstuvwxyz 555@gmail.com','2018-06-01 16:00:00','2018-06-01 16:00:00',5),(50,'jklmnopqrstuvwxyz 555@gmail.com','2018-06-01 17:00:00','2018-06-01 17:00:00',5),(51,'abcdefghijklmnopqrstuvwxyz 666@gmail.com','2018-01-01 13:00:00','2018-01-01 13:00:00',6),(52,'bcdefghijklmnopqrstuvwxyz 666@gmail.com','2018-02-01 13:00:00','2018-02-01 13:00:00',6),(53,'cdefghijklmnopqrstuvwxyz 666@gmail.com','2018-03-01 13:00:00','2018-03-01 13:00:00',6),(54,'defghijklmnopqrstuvwxyz 666@gmail.com','2018-04-01 13:00:00','2018-04-01 13:00:00',6),(55,'efghijklmnopqrstuvwxyz 666@gmail.com','2018-05-01 13:00:00','2018-05-01 13:00:00',6),(56,'fghijklmnopqrstuvwxyz 666@gmail.com','2018-06-01 13:00:00','2018-06-01 13:00:00',6),(57,'ghijklmnopqrstuvwxyz 666@gmail.com','2018-06-01 14:00:00','2018-06-01 14:00:00',6),(58,'hijklmnopqrstuvwxyz 666@gmail.com','2018-06-01 15:00:00','2018-06-01 15:00:00',6),(59,'ijklmnopqrstuvwxyz 666@gmail.com','2018-06-01 16:00:00','2018-06-01 16:00:00',6),(60,'jklmnopqrstuvwxyz 666@gmail.com','2018-06-01 17:00:00','2018-06-01 17:00:00',6),(61,'abcdefghijklmnopqrstuvwxyz 777@gmail.com','2018-01-01 13:00:00','2018-01-01 13:00:00',7),(62,'bcdefghijklmnopqrstuvwxyz 777@gmail.com','2018-02-01 13:00:00','2018-02-01 13:00:00',7),(63,'cdefghijklmnopqrstuvwxyz 777@gmail.com','2018-03-01 13:00:00','2018-03-01 13:00:00',7),(64,'defghijklmnopqrstuvwxyz 777@gmail.com','2018-04-01 13:00:00','2018-04-01 13:00:00',7),(65,'efghijklmnopqrstuvwxyz 777@gmail.com','2018-05-01 13:00:00','2018-05-01 13:00:00',7),(66,'fghijklmnopqrstuvwxyz 777@gmail.com','2018-06-01 13:00:00','2018-06-01 13:00:00',7),(67,'ghijklmnopqrstuvwxyz 777@gmail.com','2018-06-01 14:00:00','2018-06-01 14:00:00',7),(68,'hijklmnopqrstuvwxyz 777@gmail.com','2018-06-01 15:00:00','2018-06-01 15:00:00',7),(69,'ijklmnopqrstuvwxyz 777@gmail.com','2018-06-01 16:00:00','2018-06-01 16:00:00',7),(70,'jklmnopqrstuvwxyz 777@gmail.com','2018-06-01 17:00:00','2018-06-01 17:00:00',7),(71,'abcdefghijklmnopqrstuvwxyz 888@gmail.com','2018-01-01 13:00:00','2018-01-01 13:00:00',8),(72,'bcdefghijklmnopqrstuvwxyz 888@gmail.com','2018-02-01 13:00:00','2018-02-01 13:00:00',8),(73,'cdefghijklmnopqrstuvwxyz 888@gmail.com','2018-03-01 13:00:00','2018-03-01 13:00:00',8),(74,'defghijklmnopqrstuvwxyz 888@gmail.com','2018-04-01 13:00:00','2018-04-01 13:00:00',8),(75,'efghijklmnopqrstuvwxyz 888@gmail.com','2018-05-01 13:00:00','2018-05-01 13:00:00',8),(76,'fghijklmnopqrstuvwxyz 888@gmail.com','2018-06-01 13:00:00','2018-06-01 13:00:00',8),(77,'ghijklmnopqrstuvwxyz 888@gmail.com','2018-06-01 14:00:00','2018-06-01 14:00:00',8),(78,'hijklmnopqrstuvwxyz 888@gmail.com','2018-06-01 15:00:00','2018-06-01 15:00:00',8),(79,'ijklmnopqrstuvwxyz 888@gmail.com','2018-06-01 16:00:00','2018-06-01 16:00:00',8),(80,'jklmnopqrstuvwxyz 888@gmail.com','2018-06-01 17:00:00','2018-06-01 17:00:00',8),(81,'abcdefghijklmnopqrstuvwxyz 999@gmail.com','2018-01-01 13:00:00','2018-01-01 13:00:00',9),(82,'bcdefghijklmnopqrstuvwxyz 999@gmail.com','2018-02-01 13:00:00','2018-02-01 13:00:00',9),(83,'cdefghijklmnopqrstuvwxyz 999@gmail.com','2018-03-01 13:00:00','2018-03-01 13:00:00',9),(84,'defghijklmnopqrstuvwxyz 999@gmail.com','2018-04-01 13:00:00','2018-04-01 13:00:00',9),(85,'efghijklmnopqrstuvwxyz 999@gmail.com','2018-05-01 13:00:00','2018-05-01 13:00:00',9),(86,'fghijklmnopqrstuvwxyz 999@gmail.com','2018-06-01 13:00:00','2018-06-01 13:00:00',9),(87,'ghijklmnopqrstuvwxyz 999@gmail.com','2018-06-01 14:00:00','2018-06-01 14:00:00',9),(88,'hijklmnopqrstuvwxyz 999@gmail.com','2018-06-01 15:00:00','2018-06-01 15:00:00',9),(89,'ijklmnopqrstuvwxyz 999@gmail.com','2018-06-01 16:00:00','2018-06-01 16:00:00',9),(90,'jklmnopqrstuvwxyz 999@gmail.com','2018-06-01 17:00:00','2018-06-01 17:00:00',9),(91,'abcdefghijklmnopqrstuvwxyz 000@gmail.com','2018-01-01 13:00:00','2018-01-01 13:00:00',10),(92,'bcdefghijklmnopqrstuvwxyz 000@gmail.com','2018-02-01 13:00:00','2018-02-01 13:00:00',10),(93,'cdefghijklmnopqrstuvwxyz 000@gmail.com','2018-03-01 13:00:00','2018-03-01 13:00:00',10),(94,'defghijklmnopqrstuvwxyz 000@gmail.com','2018-04-01 13:00:00','2018-04-01 13:00:00',10),(95,'efghijklmnopqrstuvwxyz 000@gmail.com','2018-05-01 13:00:00','2018-05-01 13:00:00',10),(96,'fghijklmnopqrstuvwxyz 000@gmail.com','2018-06-01 13:00:00','2018-06-01 13:00:00',10),(97,'ghijklmnopqrstuvwxyz 000@gmail.com','2018-06-01 14:00:00','2018-06-01 14:00:00',10),(98,'hijklmnopqrstuvwxyz 000@gmail.com','2018-06-01 15:00:00','2018-06-01 15:00:00',10),(99,'ijklmnopqrstuvwxyz 000@gmail.com','2018-06-01 16:00:00','2018-06-01 16:00:00',10),(100,'jklmnopqrstuvwxyz 000@gmail.com','2018-06-01 17:00:00','2018-06-01 17:00:00',10);
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `id` varchar(100) NOT NULL,
  `data` text,
  `expires` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sessions_expires` (`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Aaa','Zzz','111@gmail.com','$2b$08$8G98EViJzgIDhSboQ6FOR.TuXWEEN.SCmpD33xonksa7beg3v3CQi','2018-12-29 13:58:09','2018-12-29 13:58:09'),(2,'Bbb','Zzz','222@gmail.com','$2b$08$8G98EViJzgIDhSboQ6FOR.TuXWEEN.SCmpD33xonksa7beg3v3CQi','2018-12-29 13:58:09','2018-12-29 13:58:09'),(3,'Ccc','Zzz','333@gmail.com','$2b$08$8G98EViJzgIDhSboQ6FOR.TuXWEEN.SCmpD33xonksa7beg3v3CQi','2018-12-29 13:58:09','2018-12-29 13:58:09'),(4,'Ddd','Zzz','444@gmail.com','$2b$08$8G98EViJzgIDhSboQ6FOR.TuXWEEN.SCmpD33xonksa7beg3v3CQi','2018-12-29 13:58:09','2018-12-29 13:58:09'),(5,'Eee','Zzz','555@gmail.com','$2b$08$8G98EViJzgIDhSboQ6FOR.TuXWEEN.SCmpD33xonksa7beg3v3CQi','2018-12-29 13:58:09','2018-12-29 13:58:09'),(6,'Fff','Zzz','666@gmail.com','$2b$08$8G98EViJzgIDhSboQ6FOR.TuXWEEN.SCmpD33xonksa7beg3v3CQi','2018-12-29 13:58:09','2018-12-29 13:58:09'),(7,'Ggg','Zzz','777@gmail.com','$2b$08$8G98EViJzgIDhSboQ6FOR.TuXWEEN.SCmpD33xonksa7beg3v3CQi','2018-12-29 13:58:09','2018-12-29 13:58:09'),(8,'Hhh','Zzz','888@gmail.com','$2b$08$8G98EViJzgIDhSboQ6FOR.TuXWEEN.SCmpD33xonksa7beg3v3CQi','2018-12-29 13:58:09','2018-12-29 13:58:09'),(9,'Iii','Zzz','999@gmail.com','$2b$08$8G98EViJzgIDhSboQ6FOR.TuXWEEN.SCmpD33xonksa7beg3v3CQi','2018-12-29 13:58:09','2018-12-29 13:58:09'),(10,'Jjj','Zzz','000@gmail.com','$2b$08$8G98EViJzgIDhSboQ6FOR.TuXWEEN.SCmpD33xonksa7beg3v3CQi','2018-12-29 13:58:09','2018-12-29 13:58:09'),(11,'Zzz','Aaa','a111@gmail.com','$2b$08$8G98EViJzgIDhSboQ6FOR.TuXWEEN.SCmpD33xonksa7beg3v3CQi','2018-12-29 13:58:09','2018-12-29 13:58:09'),(12,'Zzz','Bbb','b222@gmail.com','$2b$08$8G98EViJzgIDhSboQ6FOR.TuXWEEN.SCmpD33xonksa7beg3v3CQi','2018-12-29 13:58:09','2018-12-29 13:58:09');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-03 23:18:36
