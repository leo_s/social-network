const Sequelize = require('sequelize');

module.exports = (sequelize) => {
  const DriverCar = sequelize.define('DriverCar', {
    driver_id: {
      type: Sequelize.INTEGER,
      references: {
        model: 'Driver',
        key: 'id',
      }
    },
    car_id: {
      type: Sequelize.INTEGER,
      references: {
        model: 'Car',
        key: 'id',
      }
    },
  }, {
    underscored: true,
    tableName: 'driver_cars'
  });

/*
  DriverCar.associate = function (models) {
    // Define model's associations here
  };
*/

  return DriverCar;
};