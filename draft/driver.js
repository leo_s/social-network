const Sequelize = require('sequelize');

module.exports = (sequelize) => {
  const Driver = sequelize.define('Driver', {
    first_name: {
      type: Sequelize.STRING(50)
    },
    last_name: {
      type: Sequelize.STRING(50)
    },
    birth_date: {
      type: Sequelize.DATE
    }
  }, {
    underscored: true,
    tableName: 'drivers'
  });

  Driver.associate = function (models) {
    Driver.belongsToMany(models.Car, {
      through: models.DriverCar
    });
  };

  return Driver;
};