const chai = require('chai');
const chaiHttp = require('chai-http');

const {expect} = chai;
chai.use(chaiHttp);

/*global describe before it*/
describe('createProfile', function () {

    describe('POST /createProfile', function () {
        describe('when password field is absent', function () {
            before(async function () {
                this.response = await chai.request(this.app)
                    .post(`/createProfile`)
                    .send();
            });

            it('should respond with 400', function () {
                expect(this.response).to.have.status(400);
            });

            it('should respond with message', function () {
                expect(this.response.text).to.equals('Password password should be at 6-10 characters, contain at least one number, one lowercase letter, one uppercase letter, one special character');
            });
        });

        describe('when password field is present but has not a special character', function () {
            before(async function () {
                this.response = await chai.request(this.app)
                    .post(`/createProfile`)
                    .send({
                        password: '123QWEwe',
                    });
            });

            it('should respond with 400', function () {
                expect(this.response).to.have.status(400);
            });

            it('should respond with message', function () {
                expect(this.response.text).to.equals('Password password should be at 6-10 characters, contain at least one number, one lowercase letter, one uppercase letter, one special character');
            });
        });


        describe('when password is correct but name, surname, email fields are absent', function () {
            before(async function () {
                this.response = await chai.request(this.app)
                    .post(`/createProfile`)
                    .send({
                        password: '1qaz!QAZ',
                    });
            });

            it('should respond with 400', function () {
                expect(this.response).to.have.status(400);
            });

            it('should respond with message', function () {
                expect(this.response.text).to.equals('notNull Violation: User.name cannot be null,\n' +
                    'notNull Violation: User.surname cannot be null,\n' +
                    'notNull Violation: User.email cannot be null');
            });
        });


        describe('when password is correct, other fields are present but empty', function () {
            before(async function () {
                this.response = await chai.request(this.app)
                    .post(`/createProfile`)
                    .send({
                        password: '1qaz!QAZ',
                        name: '',
                        surname: '',
                        email: '',
                    });
            });

            it('should respond with 400', function () {
                expect(this.response).to.have.status(400);
            });

            it('should respond with message', function () {
                expect(this.response.text).to.equals('Validation error: Validation notEmpty on name failed,\n' +
                    'Validation error: Validation notEmpty on surname failed,\n' +
                    'Validation error: Validation isEmail on email failed,\n' +
                    'Validation error: Validation notEmpty on email failed');
            });
        });

        describe('when all fields are correct', function () {
            before(async function () {
                this.response = await chai.request(this.app)
                    .post(`/createProfile`)
                    .send({
                        password: '1qaz!QAZ',
                        name: 'Mary',
                        surname: 'Pink',
                        email: 'mary-pink@gmail.com',
                    });
            });

            it('should respond with 400', function () {
                expect(this.response).to.have.status(200);
            });

            it('should respond with message', function () {
                expect(this.response.text).to.equals('Account is created!');
            });
        });
    });

});