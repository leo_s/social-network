const Sequelize = require('sequelize');

module.exports = (sequelize) => {
  const Car = sequelize.define('Car', {
    manufacturer: {
      type: Sequelize.STRING(50)
    },
    model: {
      type: Sequelize.STRING(50)
    },
    year: {
      type: Sequelize.INTEGER,
    }
  }, {
    underscored: true,
    tableName: 'cars'
  });

  Car.associate = function (models) {
    Car.belongsToMany(models.Driver, {
      through: models.DriverCar
    });
  };

  return Car;
};